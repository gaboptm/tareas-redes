#include "lista.h"
#include <stdlib.h>
#include <stdio.h>

void imprime(void * elemento){

	char * e = (char*)elemento;
	printf("%c", *e);
}

void mayusculas(void * elemento){
	char * e = (char*)elemento;
	*e += 'A' - 'a';
}

int main(){	
	struct lista lista = {cabeza:0, longitud:0};
	
	for (int c = 'a'; c < 'k'; c++){
		char * e = (char*)malloc(sizeof(e));
		*e = c;				
		agrega_elemento(&lista, e);
		
	}
	
	printf("Longitud de la lista: %d\nElementos: ", lista.longitud);
	aplica_funcion(&lista, imprime);
	aplica_funcion(&lista, mayusculas);
	printf("\nMayusculas: ");
	aplica_funcion(&lista, imprime);
	int n = lista.longitud;
	for (int i = 0; i < n; i++){		
		printf("\nElemento a ser eliminado: %c \n", *(char*)obten_elemento(&lista, 0));	
		char * e = (char*)elimina_elemento(&lista, 0);
		printf("Elemento eliminado: %c, longitud de la lista: %d", *e, lista.longitud);
		free(e);
	}
	puts("");
	return 0;
}