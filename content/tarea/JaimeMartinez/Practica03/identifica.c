#include <stdio.h>
#include <ctype.h>
#include <string.h>

#define BYTES 8

void readFile(char *file);

void readFile(char *file)
{
  char nums[8];
  int i;
  FILE *fp = fopen(file,"rb");
  fread(nums,1,8,fp);
  fclose(fp);
  int bandera = 1;
  if(nums[0] == 0x50 && nums[1] == 0x4B && nums[2] == 0x03 && nums[3] == 0x04){
    printf(strcat(file , ": Es un archivo ZIP\n"));
    bandera = 0;
  }
  if(nums[0] == 0x50 && nums[1] == 0x4B && nums[2] == 0x05 && nums[3] == 0x06){
    printf(strcat(file , ": Es un archivo ZIP\n"));
    bandera = 0;
  }
  if(nums[0] == 0x50 && nums[1] == 0x4B && nums[2] == 0x07 && nums[3] == 0x08){
    printf(strcat(file , ": Es un archivo ZIP\n"));
    bandera = 0;
  }
  if(nums[0] == 0x47 && nums[1] == 0x49 && nums[2] == 0x46 && nums[3] == 0x38 && nums[4] == 0x37 && nums[5] == 0x61){
    printf(strcat(file , ": Es un archivo GIF\n"));
    bandera = 0;
  }
  if(nums[0] == 0x47 && nums[1] == 0x49 && nums[2] == 0x46 && nums[3] == 0x38 && nums[4] == 0x39 && nums[5] == 0x61){
    printf(strcat(file , ": Es un archivo GIF\n"));
    bandera = 0;
  }
  if(nums[0] == 0x7F && nums[1] == 0x45 && nums[2] == 0x4C && nums[3] == 0x46){
    printf(strcat(file , ": Es un archivo ELF\n"));
    bandera = 0;
  }
  if(nums[0] == 0x89 && nums[1] == 0x50 && nums[2] == 0x4E && nums[3] == 0x47 && nums[4] == 0x0D && nums[5] == 0x0A && nums[6] == 0x1A && nums[7] == 0x0A){
    printf(strcat(file , ": Es un archivo PNG\n"));
    bandera = 0;
    }
  if(nums[0] == 0xFF && nums[1] == 0xFB){
    printf(strcat(file , ": Es un archivo MP3\n"));
    bandera = 0;
  }
  if(nums[0] == 0x49 && nums[1] == 0x44 && nums[2] == 0x43){
    printf(strcat(file , ": Es un archivo MP3\n"));
    bandera = 0;
    }
  if(nums[0] == 0x4D && nums[1] == 0x5A){
    printf(strcat(file , ": Es un archivo EXE\n"));
    bandera = 0;
  }
  if(bandera){
    printf(strcat(file , ": Archivo no identificado\n"));
  }
}


int main(int argv,char *argc[])
{
  for(int tempo = argv-1; tempo>0; tempo--){
    readFile(argc[tempo]);
    }
}