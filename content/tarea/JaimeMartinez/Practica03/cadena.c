#include <stdio.h>
#include <ctype.h>
#include <string.h>

void readFile(char *file);


void readFile(char *file)
{
  char contenido;
  FILE *fp = fopen(file,"rb");
  while((contenido = fgetc(fp)) != EOF){
    if(isgraph(contenido)){
      printf("%c",contenido);
    }
  }
  fclose(fp);
}

int main(int argv,char *argc[])
{
  for(int tempo = argv-1; tempo>0; tempo--){
    readFile(argc[tempo]);
    }
}