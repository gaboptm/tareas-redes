#include<stdlib.h>
#include<dirent.h>
#include<string.h>
#include<stdio.h>
#include"lista.h"

void imprime(void * elemento) {
    char * e = (char*)elemento;
    printf("%c ", *e);
}
void imprimeS(void * elemento){
  char * e = (char*)elemento;
  printf(e);
}
void mayuscula(void * elemento) {
    char *e = (char *)elemento;
    *e += 'A' - 'a';
}
int main() {
    struct lista lista = {cabeza:0, longitud:0};
    for(int c = 'a'; c <= 'k'; c++) {
        char * e = (char*)malloc(sizeof(e));
        *e = c;
        agrega_elemento(&lista, e);
    }
    printf("Longitud de lista: %d\nElementos: ", lista.longitud);
    aplica_funcion(&lista, imprime);
    aplica_funcion(&lista, mayuscula);
    printf("\nMasyusculas: ");
    aplica_funcion(&lista, imprime);
    int n = lista.longitud;
    for(int i = 0; i < n; i++) {
        printf("\nElemento a ser eliminado: %c, ", *(char*)obten_elemento(&lista, 0));
        char * e = (char *)elimina_elemento(&lista, 0);
        printf("elemento eliminado: %c, longitud de lista: %d", *e, lista.longitud);
        free(e);
    }
    puts("");
    int bandera;
    bandera = 1;
    int menu;
    int pos;
    struct lista lista2 = {cabeza:0, longitud:0};
    while(bandera){
      printf("Selecciona una de las siguientes opciones:\n");
      printf("1 -> Guardar archivo/directorio\n");
      printf("2 -> Leer el contenido del archivo\n");
      printf("3 -> Eliminar el archivo\n");
      printf("4 -> Salir\n");
      scanf("%d",&menu);
      switch(menu){
      case 1:
	printf("Ingresa el nombre del archivo\n");
	char * name = (char*)malloc(sizeof(name));
	scanf("%s",name);
	agrega_elemento(&lista2,name);
	break;
      case 2:
    printf("Ingresa la posicion del archivo a leer\n");
	scanf("%d",&pos);
	DIR * dir1;
	char contenido;
	char * x1 = (char*)obten_elemento(&lista2,pos);
	if((dir1 = opendir(x1)) != NULL){
	  char parte1[4];
	  char parte2[50];
	  strcpy(parte1, "ls ");
	  strcpy(parte2, x1);
	  char * comando = strcat(parte1,parte2);
	  system(comando);
	}else{
	  FILE * arch1 = fopen(x1,"r");
	  while((contenido = fgetc(arch1)) != EOF){
	    printf("%c",contenido);
	  }
	  fclose(arch1);
	  }
	break;
      case 3:
	printf("Ingresa el indice del archivo a eliminar a partir de 0\n");
	scanf("%d",&pos);
	char * deleted = (char *)elimina_elemento(&lista2, pos);
	printf("Elemento eliminado: ");
	printf(deleted);
	printf("\n");
	break;
      case 4:
	bandera = 0;
	break;
      }
    }
    
}

