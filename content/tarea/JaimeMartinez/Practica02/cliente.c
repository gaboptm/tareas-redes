#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<arpa/inet.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<netdb.h>


char *trim(char *str, const char *seps)
{
    return ltrim(rtrim(str, seps), seps);
}


char *ltrim(char *str, const char *seps)
{
    size_t totrim;
    if (seps == NULL) {
        seps = "\t\n\v\f\r ";
    }
    totrim = strspn(str, seps);
    if (totrim > 0) {
        size_t len = strlen(str);
        if (totrim == len) {
            str[0] = '\0';
        }
        else {
            memmove(str, str + totrim, len + 1 - totrim);
        }
    }
    return str;
}

char *rtrim(char *str, const char *seps)
{
    int i;
    if (seps == NULL) {
        seps = "\t\n\v\f\r ";
    }
    i = strlen(str) - 1;
    while (i >= 0 && strchr(seps, str[i]) != NULL) {
        str[i] = '\0';
        i--;
    }
    return str;
}



int main(int argc, char **argv){
  if(argc<2)
  {
    printf("<host> <puerto>\n");
    return 1;
  }
  //Declaramos la estructura con información para la conexión
  struct sockaddr_in cliente;

  struct hostent *servidor;
  servidor = gethostbyname(argv[1]); 
  if(servidor == NULL)
  { //Comprobamos el host
    printf("Host erróneo\n");
    return 1;
  }
  int port, conexion;
  char buffer[100];
  //procedemos a asignar el socket
  conexion = socket(AF_INET, SOCK_STREAM, 0); 
  port=(atoi(argv[2])); 
  //llenamos con ceros la estructuraa
  bzero((char *)&cliente, sizeof((char *)&cliente)); 
  
  cliente.sin_family = AF_INET; //Procedemos con la asignacion del protocolo
  //asignamos el puerto
  cliente.sin_port = htons(puerto); 
  bcopy((char *)servidor->h_addr, (char *)&cliente.sin_addr.s_addr, sizeof(servidor->h_length));
  if(connect(conexion,(struct sockaddr *)&cliente, sizeof(cliente)) < 0)
  { //Pasamos a conectar con el host
    printf("Error conectando con el host\n");
    close(conexion);
    return 1;
  }
  printf("Conectado con %s:%d\n",inet_ntoa(cliente.sin_addr),htons(cliente.sin_port));
  printf("Comandos:");
  int bandera = 1;
  while(bandera){
    fgets(buffer, 100, stdin);
    char * comando;
    if(strcmp(buffer,"salir") == 10){
      bandera = 0;
      comando = buffer;
    } else {
      comando = strcat(trim(buffer,"\t\v\f\r\n"), " >> salida.txt");
    }
    
    send(conexion, comando, 100, 0); //envio
    bzero(buffer, 100);
    recv(conexion, buffer, 100, 0); //recepción
    printf("%s", buffer);
  }
return 0;
}
