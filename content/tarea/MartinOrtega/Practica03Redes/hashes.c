#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <openssl/md5.h>
#include <openssl/sha.h>

void MDFile (char *filename){
  FILE *inFile = fopen (filename, "rb");
  MD5_CTX mdContext;
  int bytes;
  unsigned char data[1024];
  
  if (inFile == NULL) {
    printf ("error al abrir archivo %s.\n", filename);
    return;
  }
  
  MD5_Init (&mdContext);
  while ((bytes = fread (data, 1, 1024, inFile)) != 0)
    MD5_Update (&mdContext, data, bytes);
  MD5_Final (data,&mdContext);
  for(int n=0; n<MD5_DIGEST_LENGTH; n++)
        printf("%02x", data[n]);
  printf (" %s\n", filename);
  fclose (inFile);
  }

void SHA1File (char *filename){
  FILE *inFile = fopen (filename, "rb");
  SHA_CTX mdContext;
  int bytes;
  unsigned char data[1024];
  
  if (inFile == NULL) {
    printf ("error al abrir archivo %s.\n", filename);
    return;
  }
  
  SHA1_Init (&mdContext);
  while ((bytes = fread (data, 1, 1024, inFile)) != 0)
    SHA1_Update (&mdContext, data, bytes);
  SHA1_Final (data,&mdContext);
  for(int n=0; n<SHA_DIGEST_LENGTH; n++)
        printf("%02x", data[n]);
  printf (" %s\n", filename);
  fclose (inFile);
  }

void SHA256File (char *filename){
  FILE *inFile = fopen (filename, "rb");
  SHA256_CTX mdContext;
  int bytes;
  unsigned char data[1024];
  
  if (inFile == NULL) {
    printf ("error al abrir archivo %s.\n", filename);
    return;
  }
  
  SHA256_Init (&mdContext);
  while ((bytes = fread (data, 1, 1024, inFile)) != 0)
    SHA256_Update (&mdContext, data, bytes);
  SHA256_Final (data,&mdContext);
  for(int n=0; n<SHA256_DIGEST_LENGTH; n++)
        printf("%02x", data[n]);
  printf (" %s\n", filename);
  fclose (inFile);
  }

void hashFile(char *hash,char *file){
  char contenido;
  if(strcmp(hash,"md5") == 0){
    MDFile(file);
  }
  if(strcmp(hash,"sha1") == 0){
    SHA1File(file);
  }
  if(strcmp(hash,"sha256") == 0){
    SHA256File(file);
  }
  /*FILE *fp = fopen(file,"rb");
  while((contenido = fgetc(fp)) != EOF){
    if(isgraph(contenido)){
      printf("%c",contenido);
    }
  }
  fclose(fp);*/
}

int main(int argv,char *argc[])
{
  for(int tmp = argv-1; tmp>1; tmp--){
    hashFile(argc[1],argc[tmp]);
    }
}
