#include <stdlib.h>
#include <stdio.h>
#include <pcap.h>
/*Compilar gcc inyector.c -lpcap
Ejecutar ./a*/

void main(int argc, char **argv)
{
pcap_t *fp;
char errbuf[PCAP_ERRBUF_SIZE];
u_char packet[100];
int i;

    /* Se checa que cumpla las caracteristicas*/
    if (argc != 2)
    {
        printf("usage: %s interface (e.g. 'rpcap://eth0')", argv[0]);
        return;
    }
    if ( (fp= pcap_open_live(argv[1],// nombre de la interface
                        100,
                        100,
                        1000,
                        errbuf// error buffer
                        ) ) == NULL)
    {
        fprintf(stderr,"\nUnable to open the adapter. %s is not supported\n", argv[1]);
        return;
    }

    /* Asignamos mac destino 1:1:1:1:1:1 */
    packet[0]=1;
    packet[1]=1;
    packet[2]=1;
    packet[3]=1;
    packet[4]=1;
    packet[5]=1;

    /*  mac inicio  2:2:2:2:2:2 */
    packet[6]=2;
    packet[7]=2;
    packet[8]=2;
    packet[9]=2;
    packet[10]=2;
    packet[11]=2;

    for(i=12;i<100;i++)
    {
        packet[i]=i%256;
    }

    /* Enviamos el paquete */
    if (pcap_sendpacket(fp, packet, 100 ) != 0)
    {
        fprintf(stderr,"\nError sending the packet: \n", pcap_geterr(fp));
        return;
    }

    return;
}
