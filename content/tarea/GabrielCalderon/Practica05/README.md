#Práctica 5 
## Integrantes:
* Cova Pacheco Felipe de Jesús
* Calderón Fernández Gabriel

1) 

2) DNC

![Imagen 1](DNC1.png)
![Imagen 2](DNC2.png)
![Imagen 3](DNC3.png)
![Imagen 4](DNC4.png)
![Imagen 5](DNC5.png)
![Imagen 6](DNC6.png)
![Imagen 7](DNC7.png)
![Imagen 8](DNC8.png)
![Imagen 9](DNC9.png)
![Imagen 10](DNC10.png)

Pregunta 1. Qué es un servidor DNS?

Es un dispositivo conectado a Redes IP que hace traducciones entre nombres de dominio a direcciones IP, con base en su base de datos y la configuración que este tenga para logras resolver esas traducciones.

Pregunta 2. ¿Para qué se utiliza cada uno de los registros SOA, NS, A, AAAA, PTR, CNAME, MX y TXT?

SOA: proporciona información sobre el servidor DNS primario de la zona.
NS: Define la asociación que existe entre un nombre de dominio y los servidores de nombres que almacenan la información de dicho dominio.
A: se usa para traducir nombres de servidores de alojamiento a direcciones IPv4.
AAAA: se usa para traducir nombres de host a direcciones IPv6.
PTR: tiene la función inversa del registro A, traduciendo de una dirección IP a un nombre de dominio.
CNAME: crea nombres de servidores de alojamiento adicionales para los servidores de alojamiento de un dominio,(canonical name).
MX: ​Un ​registro MX​ (del inglés ​Mail eXchange record​, en español "registro de intercambio de correo") es un tipo de registro, un recurso ​DNS​ que especifica cómo debe ser encaminado un correo electrónico​ en ​internet​. Los registros MX apuntan a los ​servidores​ a los cuales envían un correo electrónico, y a cuál de ellos debería ser enviado en primer lugar, por prioridad.
TXT: contiene información para fuentes externas al dominio.

Pregunta 3. ¿Qué hacen las herramientas nslookup y dig?

nslookup: se utiliza para saber si el DNS está resolviendo correctamente los nombres y las IPs, con este se puede obtener la dirección IP desde el nombre de dominio o viceversa. 
dig: (domain information groper) permite realizar consultas a los distintos registros DNS.

3) Packet Tracer

![Imagen 1](1.jpg)
![Imagen 2](2.jpg)
![Imagen 3](3.jpg)
![Imagen 4](4.jpg)
![Imagen 5](5.jpg)
![Imagen 6](6.jpg)
![Imagen 7](7.jpg)
![Imagen 8](8.jpg)
![Imagen 9](9.jpg)
![Imagen 10](10.jpg)
![Imagen 11](11.jpg)
![Imagen 12](12.jpg)
![Imagen 13](13.jpg)
![Imagen 14](14.jpg)
![Imagen 15](15.jpg)
![Imagen 16](16.jpg)
![Imagen 17](17.jpg)
![Imagen 18](18.jpg)
![Imagen 19](19.jpg)
![Imagen 20](20.jpg)
![Imagen 21](21.jpg)
![Imagen 22](22.jpg)
![Imagen 23](23.jpg)
![Imagen 24](24.jpg)

Pregunta 1. ¿Qué diferencia hay entre un switch y un router?

Un switch solo transfiere los paquetes de manera diferencial entre un equipos, mientras que el router,permite dirigir un paquete entre distintas redes para hacerlo llegar a su destino final.

Pregunta 2. ¿Qué tipos de switches hay?

Los interruptores mecánicos​ deben activarse físicamente, moviendo, presionando, soltando o tocando sus contactos.
    
Los interruptores electrónicos​ no requieren ningún contacto físico para controlar un circuito. Estos son activados por la acción del semiconductor.

Pregunta 3. ¿Qué es una lista de acceso y para qué se utilizan?

Una lista de control de acceso (ACL) consiste en una o más entradas de control de acceso (ACE) que definen colectivamente el perfil de tráfico de la red. Este perfil puede luego ser referenciado por las características del software del software Cisco IOS XR , como el filtrado del tráfico, la creación de colas de prioridad o personalizada y el control de acceso dinámico. Cada ACL incluye un elemento de acción (permitir o negar) y un elemento de filtro según criterios como la dirección de origen, la dirección de destino, el protocolo y los parámetros específicos del protocolo.

Pregunta 4. Se creó una lista de acceso con el comando access-list 1 permit 192.168.1.0 0.0.0.255, la última parte del comando (0.0.0.255) es una wildcard, ¿qué es una wildcard y en qué se diferencia con una máscara de red?

Una wildcard una máscara de bits que indica qué partes de una dirección de IP son relevantes para la ejecución de una acción , sirve para indicar qué direcciones IP tendrían que ser permitidas o denegadas .
Wildcard es una máscara de red invertida y nos indica el numero de Host que en una IP, mientras que mascara de red nos dice el numero de Redes en una IP

Pregunta 5. ¿Qué diferencia hay entre una dirección IP pública y una privada?

Una IP pública sirve para hacer la identificación de un dispositivos a nivel de diferentes redes.
IP privada es una dirección que sirve para identificar a un dispositivo dentro de una red.

Pregunta 6. ¿Qué diferencia hay entre NAT estática, NAT dinámica y PAT? ¿Cuál es la que se encuentra usualmente en los routers de los hogares?

NAT Estático se mapea una dirección IP privada con una dirección IP pública de forma estática. De esta manera, cada equipo en la red privada debe tener su correspondiente IP pública asignada para poder acceder a Internet.
NAT Dinámico utiliza un pool de IPs públicas para un pool de IPs privadas que serán mapeadas de forma dinámica y a demanda.
PAT Es una característica del estándar NAT, que traduce conexiones TCP y UDP hechas por un host y un puerto en una red externa a otra dirección y puerto de la red interna

