#include <stdio.h>           /* for printf and fprintf() */
#include <sys/socket.h>      /* for socket(), bind() and inet_ntoa() */
#include <arpa/inet.h>       /* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>          /* for atoi() */
#include <string.h>          /* for memset() */
#include <unistd.h>          /* for close() */
#include "HandleTCPClient.h"
#define MAXPENDING 5         /* Maximum outstanding connection requests */

void DieWithError(char *errorMessage);  /* Error handling function */
void HandleTCPClient(int clntSocket);   /* TCP client handling function */


int main(int argc, char *argv[]){
  
  int servSock;                /* Socket descriptor for server */
  int clntSock;                /* Socket descriptor for client */
  struct sockaddr_in servAddr; /* Local address */
  struct sockaddr_in clntAddr; /* Client address */
  unsigned short servPort;     /* Server port */
  unsigned int clntLen;        /* Length of client address data structure */

  if (argc != 2){ /* Test for correct number of arguments */

    fprintf(stderr, "Usage: %s <Server Port>\n", argv[0]);
    exit(1);
  }

  servPort = atoi(argv[1]);

  /* Create socket for incoming connections */
  if((servSock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
    DieWithError("socket() failed");

  /* Construct local address structure */
  memset(&servAddr, 0, sizeof(servAddr));       /* Zero out structure */
  
  servAddr.sin_family      = AF_INET;           /* Internet address family */
  servAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
  servAddr.sin_port        = htons(servPort);   /* Local port */


  /* Bind to the local address */
  if(bind(servSock, (struct sockaddr *)&servAddr, sizeof(servAddr)) < 0)
    DieWithError("bind() failed");

  /* Mark the socket so it will listen for incoming connections */
  if(listen(servSock, MAXPENDING) < 0)
    DieWithError("listen() failed");

  for(;;){  /* Run forever D: */

    /* Set the size of the in-out parameter */
    clntLen = sizeof(clntAddr);
    /* Wait for a client to connect */
    if((clntSock = accept(servSock, (struct sockaddr *) &clntAddr, &clntLen)) < 0)
      DieWithError("accept()failed");

    /* clntSock is connected to a client */
    printf("------------------------------- \n");
    printf("Handling client %s:%i\n",
	   inet_ntoa(clntAddr.sin_addr), /* client IP addr */
	   ntohs(clntAddr.sin_port)      /* client port */
      );


  /* Function that handles the client */
  HandleTCPClient(clntSock);
  }

/* NOT REACHED */

}
