#include <stdio.h>        /* for printf() and fprintf() */
#include <sys/socket.h>   /* for recv() and send() */
#include <unistd.h>       /* for close()  */
#include <string.h>
#include "MessagesDefinition.h"
#include "DieWithError.h"

#define RCVBUFSIZE 5004     /* Size of receive buffer */

void DieWithError(char *errorMessage); /* Error handling function */

void HandleTCPClient(int clntSocket){

  int bytesSend;                  /* Bytes sended to client */
  char echoBuffer[RCVBUFSIZE];    /* Buffer for echo string */
  int totalBytesRecv;             /* Total number of bytes received */
  int bytesRecv;                  /* Bytes received on a single message */
  unsigned int strLen;            /* Command String length */
  char * str;                     /* Command String */
  unsigned int serverResponseLen; /* Length of the server response string */
  char * serverResponse;          /* For storing response on a string way */
  msgTransaction * msgResponse;    /* Structure to send as a server response */  

  /* Receive data from client until the whole data is received */
  totalBytesRecv = 0;
  while(totalBytesRecv < sizeof(msgTransaction)){
    if ((bytesRecv = recv(clntSocket, echoBuffer, RCVBUFSIZE, 0)) < 0)
      DieWithError("recv() failed");
    totalBytesRecv += bytesRecv;
    printf("Bytes received = %d\n",bytesRecv);
  } 

  
  /* Now we extract the information of the message received */
  
  msgTransaction *msgTran = (struct msgTransaction *)echoBuffer;   /* Cast echoBuffer (with received data) to msgTransaction structure */
  strLen         = *((int*)msgTran->strLen);                       /* Cast the number of bytes to read */
  str            = (char *)malloc(strLen * sizeof(char));          /* Allocate the correct amount of memory */
  memcpy(str,msgTran->str,strLen - 1);                             /* Copy the string */
  
  /* Execute the command and store the result in the output.txt file*/  
  
  char command [strLen * sizeof(char) + 50];
  sprintf(command,"%s 2>&1 | tee output.txt",str);
  printf("\nServer response to send: \n\n");
  system(command);

  /* Read form output.txt and store it on server response variable */

  FILE * fp;
  //Get output size
  
  if((fp = fopen("output.txt", "r")) == 0)
    DieWithError("fopen() failed");
  fseek(fp, 0, SEEK_END);
  serverResponseLen = ftell(fp);
  fseek(fp, 0, SEEK_SET);
  
  //Store output

  serverResponse = (unsigned char *)malloc(serverResponseLen);
  if(!serverResponse){
    DieWithError("malloc() failed");
  }
  fread(serverResponse,serverResponseLen,sizeof(unsigned char),fp);	
  
  /* Create a struct type of msgTransaction:
     reserve memory and cast to msgTransaction type */
  msgResponse = (struct msgTransaction *)malloc(sizeof(msgTransaction));
  
  /* Copy values from  input to msgTran */
  memcpy(msgResponse->strLen, &serverResponseLen, sizeof(msgResponse->strLen));
  memcpy(msgResponse->str, serverResponse, sizeof(msgResponse->str));
  
  /* Send message to client */
  if((bytesSend = send(clntSocket, msgResponse, sizeof(msgTransaction), 0)) != sizeof(msgTransaction))
    DieWithError("send() sent a different number of bytes than expected");
  else
    printf("\nBytes send to the client: %i \n", bytesSend);
  
  close(clntSocket);     /* Close client socket. 
			    El servidor solicita el cierre 
			    de la conexión con el cliente. */
}
