# PokemonSockets
Sockets para ejecutar comandos de manera remota

Para compilar ejecutar `make` en el directorio del proyecto
Se puede usar `make clean` para eliminar los archivos ejecutables creados

Para ejecutar el servidor usar `./servidor 54321` usando como ejemplo el puerto 54321			

Para ejecutar el cliente usar `./cliente 127.0.0.1 54321`

El servidor informará continuamente sobre el número de bytes enviados y recibidos así como
las respuestas que envía, el cliente se comportará igualmente. Es necesario aclarar que
*esta aplicacion solo sirve para ejecutar comandos de una sola interacción* comandos de
varias interacciones como por ejemplo __una consola de python__ causaran una falla en la
comunicación o comportamiento errático.

Práctica hecha con la ayuda del libro *TCP/IP Sockets in C: Practical Guide for Programmers* de
*Michael J. Donahoo* y *Kenneth L. Calvert*

Algunas capturas de comandos de prueba:

![alt text](img/img1.png "Envío de comando uname -r")
![alt text](img/img2.png "Ejecución de make y respuesta del servidor a uname -r")
![alt text](img/img3.png "Envío de comando inexistente")
![alt text](img/img4.png "Respuesta a comando inexistente")
![alt text](img/img5.png "Envío de comando cowsay y fin de la comunicación")	       		 