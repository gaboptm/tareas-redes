#include <stdio.h>               /* for printf() and fprintf() */
#include <sys/socket.h>          /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>           /* for sockaddr_in and inet_addr */
#include <stdlib.h>              /* for atoi() */
#include <string.h>              /* for memset() */
#include <unistd.h>              /* for close() */
#include <sys/types.h>
#include "DieWithError.h"
#include "MessagesDefinition.h"

#define RCVBUFSIZE 5100  /* Size of receive buffer (bytes) */

void DieWithError(char *errorMessage);    /* Error handling function */

int main(int argc, char *argv[]){

  int sock;                      /* Socket descriptor */
  struct sockaddr_in servAddr;   /* Server address */
  unsigned short servPort;       /* Server port */
  char *servIP;                  /* server IP address (dotted quad) */
  char *echoString;              /* String to send to server */
  char echoBuffer[RCVBUFSIZE];   /* Buffer for string */
  unsigned int echoStringLen;    /* Length of string  */
  int totalBytesRecv;            /* Total amount of bytes received gathering all messages */
  int bytesRecv, bytesSend;      /* Bytes read in single recv()
				    and bytes send */
  unsigned int strLen;           /* Length of input String*/
  char * str;                    /* Input String */

  unsigned int serverResponseLen;           /* Length of server response string*/
  char * serverResponse;                    /* server response string */
  
  
  if(argc != 3){   /* Test for correct number of arguments */
    fprintf(stderr, "Usage: %s <Server IP> <Port>  \n", argv[0]);
    exit(1);
  }
  
  for(;;){
    
    printf("Escribe un 'EOF' para terminar\n");
    
    /* Get command from user */
    
    str = (char *)malloc(5000 * sizeof(char));
    fgets(str,5000,stdin);

    /* If 'EOF' or empty input received terminate execution */
    if(strcmp(str,"'EOF'\n") == 0){
      printf("Adiós\n");      
      exit(0);
    }else if(strlen(str) < 2){
      printf("Entrada vacía, intente de nuevo\n");
      exit(0);
    }
    
    strLen = strlen(str);    
    sprintf(str,"%-*s",4999,str);
    str[5000] ='\0';

    
    
    /* Now begin the process of connection */
    
    servIP         = argv[1];                              /* First arg: server IP address (dotted quad) */
    servPort       = atoi(argv[2]);                        /* Use given port. Cast string to integer */

    /* Create a reliable, stream socket using TCP */
    if((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
      DieWithError("socket() failed");

    /* Construct the server address structure */
    memset(&servAddr, 0, sizeof(servAddr));       /* Zero out structure */
    servAddr.sin_family      = AF_INET;
    servAddr.sin_addr.s_addr = inet_addr(servIP); /* Server IP address */
    servAddr.sin_port        = htons(servPort);   /* Server port */


    /* Establish the connection to the server */
    if(connect(sock, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)
      DieWithError("connect() failed");

  
    echoStringLen = sizeof(msgTransaction);  /* Determine struct length */    
  
    
    /* Create a struct type of msgTransaction:
       reserve memory and cast to msgTransaction type */
    msgTransaction *msgTran = (struct msgTransaction *)(unsigned char *)malloc(sizeof(unsigned char));

    /* Copy values from user input to msgTran */
    memcpy(msgTran->strLen, &strLen, sizeof(msgTran->strLen));
    memcpy(msgTran->str, str, sizeof(msgTran->str));
  
    
    /* Send the data to the server */
    if((bytesSend = send(sock, msgTran, sizeof(msgTransaction), 0)) != echoStringLen)
      DieWithError("send() sent a different number of bytes than expected");
    else
      printf("Bytes send to the server: %i \n", bytesSend);
  
    /* Receive data from server until the whole data is received */
    totalBytesRecv = 0;
    while(totalBytesRecv < sizeof(msgTransaction)){
      if ((bytesRecv = recv(sock, echoBuffer, RCVBUFSIZE, 0)) < 0)
	DieWithError("recv() failed");
      totalBytesRecv += bytesRecv;
      printf("Bytes received = %d\n",bytesRecv);
    } 
  
  
    /* Now we extract the information of the message received */
  
    msgTransaction *msgRecv   = (struct msgTransaction *)echoBuffer;     /* Cast echoBuffer (with received data) to msgTransaction structure */
    serverResponseLen         = *((int*)msgRecv->strLen);                       /* Cast the number of bytes to read */
    serverResponse            = (char *)malloc(strLen * sizeof(char));          /* Allocate the correct amount of memory */
    memcpy(serverResponse,msgRecv->str,serverResponseLen);                                 /* Copy the string */
    serverResponse[serverResponseLen] = '\0';                                              /* Add the end of string */
  
    printf("\nServer response: \n");
    printf("%s", serverResponse);              /* Print the server response */
    printf("\n");                              /* Print a final linefeed */
  

    /* Close the connection */
    close(sock);
  }
  exit(0);
}
