#include <stdio.h>
#include <sys/stat.h>
#include <string.h>
#include <dirent.h>
#include "lista.c" /* para manejo de listas *}/


/* Función para imprimir el contenido de la lista de archivos */
 
void imprime(void * elemento){
  char ** e = (char**)elemento;
  printf("%s\n", *e);
}

void main(){
  char sinput [100];                               /* La cadena para guardar la entrada */
  int input;                                       /* La entrada convertida en entero */
  struct lista lista = {cabeza:0, longitud:0};     /* La lista a usar */
    
  while(1){      /* Se ejecuta infinitamente */
    system("@cls||clear"); /* Primero limpiamos la pantalla */
    printf("\n1.Insertar archivo\n2.Leer archivo\n3.Eliminar archivo\n4.Imprimir archivos\n¿Qué deseas hacer?: ");  /* Se imprime el menú */
    fgets(sinput,100,stdin);           /* Obtener la cadena de entrada */
    input = atoi(sinput);              /* Convertir a entero */
    if( input < 1 || input > 4 )       /* En caso de error reportarlo */
      fprintf(stderr,"Error: Solo enteros del 1 al 4 son válidos");
    else if(input == 1){     /* INSERTAR */

      printf("\nIntroduce el nombre del archivo: ");
      char * name = (char*)malloc(100 * sizeof(char));   /* Apartar memoria para la entrada */
      fgets(name,100,stdin);                             /* Llenar la cadena de entrada de la consola */
      name[strlen(name)-1]='\0';                         /* fgets se comporta raro en el carácter final así que cambiamos '\n' por '\0' */
      char ** elem = (char**)malloc(sizeof(elem));       /* Apartar memoria para el elemento a agregar */
      *elem = name;                                      /* Elemento ahora es lo recibido en consola*/
      agrega_elemento(&lista,elem);                      /* Agregar el elemento */
      
    }else if(input == 2){    /* LEER */
      
      printf("\n¿Cuál es el índice del archivo a leer?(entradas inválidas serán 0): "); 
      fgets(sinput,100,stdin);                          /* Obtener la cadena de entrada */
      input = atoi(sinput);                             /* Convertir a entero */
      if( input < 0 || input >= lista.longitud ){       /* En caso de error reportarlo */
	fprintf(stderr,"Error: índice fuera de rango");
	getchar();
      }else{       /* Si todo es válido tratamos de desplegar el contenido */

	/* Todo este pedazo hace uso de stat() para revisar si la cadena se refiere a un archivo o directorio */
	
	struct stat *buf = malloc(sizeof(struct stat));
	char * filename = (char*)malloc(100 * sizeof(char));
	filename = *((char**)obten_elemento(&lista,input));
	stat(filename, buf);
	if(S_ISDIR(buf->st_mode)){        /* Es un directorio */
	  
	  printf("El contenido del directorio es:\n");
	  
	  DIR *dp;                              /* apuntador a directorio */
	  struct dirent *dir;                   /* apuntador a la estructura que nos dará la información */
	  dp = opendir(filename);               /* abrimos el directorio solicitado */
	  while ((dir = readdir(dp)) != NULL) {
	    printf("%s\n", dir->d_name);        /* imprimiendo el contenido... */
	  }
	  closedir(dp);                         /* cerramos el directorio */

	}else if(S_ISREG(buf->st_mode)){  /* Es un archivo */
	  
	  printf("El contenido del archivo es: \n");
	  char ch;                        /* Caracter de lectura */
	  FILE *fp;                       /* Apuntador al archivo */
	  fp = fopen(filename, "r");      /* Abrimos en modo lectura */	  	  	  
	  while((ch = fgetc(fp)) != EOF)  /* Mientras se pueda... */
	    printf("%c", ch);	          /* Imprime lo que veas */
	  fclose(fp);                     /* Cerramos el archivo */
	  
	}else{
	  fprintf(stderr,"Error al abrir \"%s\": tipo no soportado o el archivo/directorio no existe\n",filename);
	}	
	getchar();
      }
      
    }else if(input == 3){    /* ELIMINAR */
      
      printf("\n¿Cuál es el índice del archivo a eliminar?(entradas inválidas serán 0): "); 
      fgets(sinput,100,stdin);                          /* Obtener la cadena de entrada */
      input = atoi(sinput);                             /* Convertir a entero */
      if( input < 0 || input >= lista.longitud ){       /* En caso de error reportarlo */
	fprintf(stderr,"Error: índice fuera de rango");
	getchar();
      }else{
	elimina_elemento(&lista,input);                  /* Si todo es válido procedemos a eliminar */
      }
    }else{                   /* IMPRIMIR */
      if(lista.longitud == 0){
	printf("¡Lista vacia!");
      }else{
      printf("Elementos:\n");
      aplica_funcion(&lista,imprime);
      }
      getchar();
      
    }//else
  }//while
}//main
