Para compilar todo lo necesario usar el comando ``make``, después usar:

``
$./lista
``

Para ejecutar el archivo de prueba dado en el pdf, también usar:

``
$./archivos
``

Para desplegar el menú infinito de la segunda parte, para salir de este usar ``ctrl + c``

se pueden borrar los ejecutables utilizando ``make clean``

Algunas imágenes de prueba:

![alt text](img/img1.png "Ejecución de make y lista")
![alt text](img/img3.png "Imprimiendo la lista de archivos con 2 agregados")
![alt text](img/img2.png "Mostrando el contenido de lista.h")
![alt text](img/img4.png "Mostrando el contenido del directorio testdir")
