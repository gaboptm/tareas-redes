#include "lista.h"
#include<stdlib.h>
#include<stdio.h>

/* Función que agrega un elemento al final de la lista */

void agrega_elemento(struct lista * lista, void * elemento){
  
  struct nodo * nuevo =(struct nodo*)malloc(sizeof(struct nodo));   /* Creamos el nuevo nodo  con el elemento dado*/
  nuevo->elemento = elemento;
  
  if(lista->longitud == 0){                                         /* Si la lista es nueva inicializamos */
    
    lista->cabeza = nuevo;
    
  }else{                                                            /* Si no solo  pegamos al final */
    struct  nodo * actual = lista->cabeza;
    for(int i  = 1 ; i < lista->longitud ; i++){
      actual = actual->siguiente;
    }
    actual->siguiente = nuevo;
    
  }
  
  lista->longitud++;                                                /* Actualizamos los elementos */
}

/* Función que regresa el puntero al elemento del nodo en la posición n de la lista
   si n está fuera de rango solo regresa 0
 */

void * obten_elemento(struct lista * lista, int n){
  
  if (n < 0 || n >= lista->longitud)      /* Si n fuera de rango regresamos 0 */
    return 0;
  
  struct  nodo * actual = lista->cabeza;  /* El puntero que hace el recorrido hasta n */
  for(int i  = 0 ; i < n ; i++)
    actual = actual->siguiente;
  
  return actual->elemento;                /* Regresamos el elemento del puntero posicionado en la n-ésima posición */
}

/* Elimina el n-ésimo elemento de la lista, si n en rango regresa el elemento eliminado, de lo contrario regresa 0 */

void * elimina_elemento(struct lista * lista, int n){
  if (n < 0 || n >= lista->longitud)       /* Si n fuera de rango regresa 0 */
    return 0;
  if(n == 0){                               /* Si se desea eliminar la cabeza solo actualizamos este valor  */
    void * ret = lista->cabeza->elemento;     /* El elemento a regresar */ 
    lista->cabeza = lista->cabeza->siguiente;   /* Actualizamos la cabeza */
    lista->longitud--;                      /* Actualizamos la longitud */
    return ret;
  }

  /* Si se quiere la cola solo regresamos el elemento y actualizamos la longitud,
     no se mueven los apuntadores siguientes porque la longitud delimita que tan
     lejos llega una busqueda
   */
  
  if(n == lista->longitud - 1){             
    void * ret = obten_elemento(lista,n);
    lista->longitud--;
    return ret;
  }


  /* Caso en el que el elemento a eliminar tiene anterior y siguiente*/
  
  struct  nodo * anterior = lista->cabeza;       /* El puntero al nodo anterior al target */
  for(int i  = 0 ; i < n - 1 ; i++)
    anterior = anterior->siguiente;
  struct nodo * target = anterior->siguiente;      /* El puntero al nodo objetivo o "target" */
  struct nodo * siguiente = target->siguiente;   /* El nodo siguiente al target */
  void * ret = target->elemento;                 /* El valor a regresar */

  /* Actualizamos los apuntadores */
  anterior->siguiente = target->siguiente;

  return ret;
}

void aplica_funcion(struct lista * lista, void (*f)(void *)){
  struct  nodo * actual = lista->cabeza;
  for(int i  = 0 ; i < lista->longitud ; i++){
    f(actual->elemento);
    actual = actual->siguiente;
  }
}


