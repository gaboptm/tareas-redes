#include "lista.h"
#include <stdio.h>
#include <stdlib.h>

/* Agrega un elemento al final de la lista. */
void agrega_elemento(struct lista * lista, void * elemento){
  // n va a ser el nuevo nodo
  // t va a ser un nodo auxiliar
  struct nodo *n, *t;

  // Reservamos memoria para el nuevo nodo
  n = (struct nodo*) malloc(sizeof(struct nodo));
  // Asignamos el elemento del nodo
  n -> elemento = elemento;
  // Asignamos el siguiente del nuevo último como NULL
  n -> siguiente = NULL;

  // Caso donde la lista es vacía
  if(lista -> longitud == 0)
    lista -> cabeza = n;
  else { // La lista tiene al menos un nodo
    // Nos paramos en la cabeza de la lista
    t = lista -> cabeza;

    // Nos vamos al último nodo de la lista para poder agregar al final
    while(t -> siguiente != NULL)
      t = t -> siguiente;

    // Asignamos el como siguiente del viejo último al nuevo nodo
    t -> siguiente = n;
  }

  // Aumentamos la longitud
  lista -> longitud++;
}

/* Permite obtener el n-elemento de la lista, empezando desde 0. */
void * obten_elemento(struct lista * lista, int n){
  // Nos paramos en la cabeza de la lista
  struct nodo *t = lista -> cabeza;

  // Apuntador que regresaremos
  void * elemento;

  // Iteramos sobre la lista
  for(int i = 0; i < lista -> longitud; i++){
    if(i == n){ //Llegamos al elemento que queremos
      // Asignamos lo que queremos regresar
      elemento = t -> elemento;
      break;
    }

    // Si aún no llegamos, nos vamos al siguiente
    t = t -> siguiente;

  }

  return elemento;
}

/* Elimina el n-elemento de la lista, empezando desde 0. */
void * elimina_elemento(struct lista * lista, int n){

  // Nodos para iterar la lista
  struct nodo *t, *u;

  // Apuntador que regresaremos
  void * elemento;

  if (lista -> longitud < n)
    return elemento;

  // Nos paramos en la cabeza de la lista
  t = lista -> cabeza;

  // Si el elemento que queremos eliminar es el primero
  if(n == 0){
    // Guardamos el elemento de la cabeza
    elemento = t -> elemento;

    // Asignamos la nueva cabeza
    lista -> cabeza = t -> siguiente;

    // Decrementmos la longitud de la lista
    lista -> longitud--;

    // Liberamos la memoria de la anterior cabeza
    free(t);

    return elemento;
  }

  // Iteramos la lista
  for(int i = 0; i < lista -> longitud; i++){
    if(i == (n - 1)){ // Llegamos al anterior del que queremos eliminar
      // Necesitamos algo que apunte al que queremos eliminar
      u = t -> siguiente;

      // Asignamos lo que queremos regresar
      elemento = u -> elemento;

      // Asignamos el nuevo siguiente
      t -> siguiente = u -> siguiente;

      // Liberamos la memoria del eliminado
      free(u);

      // Decrementamos la longitud de la lista
      lista -> longitud--;
      
      break;
    }

    // Si aún no llegamos seguimos iterando la lista
    t = t -> siguiente;
  }

  return elemento;

}

/* Aplica la función f a todos los elementos de la lista. */
void aplica_funcion(struct lista * lista, void (*f)(void *)){

  struct nodo *t = lista -> cabeza;

  for(int i = 0; i < lista -> longitud; i++){
    f(t -> elemento);
    t = t -> siguiente;
  }

}
