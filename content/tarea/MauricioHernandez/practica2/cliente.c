#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <string.h> // Para usar cosas como strlen, strcmp
#include <arpa/inet.h> // Para usar inet_pton
#include <unistd.h>

int main(int argc, char *argv[]){

  // Variables
	int sockfd, port, pton_ret;
  struct sockaddr_in serv_addr;
	char input[512] = {0};
	char command[512] = {0};

  // Validamos que nos pasen la IP y el puerto
	if(argc < 3){
    fprintf(stderr, "Falta el la IP del servidor o el puerto.\n");
    return 1;
  }

  // Creamos el socket
  if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
    fprintf(stderr, "Error al crear el socket\n");
    return 1;
  }
  printf("Socket creado correctamente\n");

  // Convertimos a int el puerto
  port = atoi(argv[2]);

  // Cocinamos la estructura sockaddr_in
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(port);

  // inet_pton convierte IPv4 e IPv6 de texto a binario
	pton_ret = inet_pton(AF_INET, argv[1], &serv_addr.sin_addr);

  // Verificamos que no haya habido errores con inet_pton
  switch (pton_ret) {
    // No se pudo convertir la IP
    case -1:
      fprintf(stderr, "No se pudo convertir la IP\n");
      return 1;

    // La IP era inválida
    case 0:
      fprintf(stderr, "IP inválida\n");
      return 1;
  }

  // Checamos que se haya podido conectar
	if(connect(sockfd, (struct sockaddr*) &serv_addr, sizeof(serv_addr)) < 0)
		fprintf(stderr, "Falló la conexión con el servidor\n");

	printf("Para terminar la conexión escribe 'EOF'\n");

  for(;;){

    // Limpiamos la memoria donde está input
    bzero(input, sizeof(input));
    // Limpiamos la memoria donde está command
    bzero(command, sizeof(command));

    // Si nos pasan la cadena EOF terminamos
    if (strcmp(command, "EOF") == 0)
      break;

		fgets(command, sizeof(command) - 1, stdin);

		if( command[strlen(command) - 1] == '\n')
    		command[strlen(command) - 1] = 0;

    // Escribimos
		write(sockfd, command, strlen(command));
    // Leemos
		read(sockfd, input, sizeof(input) - 1);
    // Imprimimos
		printf("%s",input);
	}

	printf("Se ha terminado la conexión\n");
  return 0;

}
