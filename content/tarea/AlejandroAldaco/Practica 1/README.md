# Práctica 1: Programación en C

Vargas Aldaco Alejandro

-----

Compilo los archivos

![01](https://gitlab.com/alex.aldaco/tareas-redes/raw/master/content/tarea/AlejandroAldaco/Practica%201/img/01.png)

### 1. Lista enlazada

![02](img/02.png)

### 2. Manejo de archivos

![archivos1](img/archivos1.png)

![archivos2](img/archivos2.png)

### e1. Bubble Sort con apuntadores

![03](https://gitlab.com/alex.aldaco/tareas-redes/raw/master/content/tarea/AlejandroAldaco/Practica%201/img/03.png)

### e2. /etc/passwd

![04](https://gitlab.com/alex.aldaco/tareas-redes/raw/master/content/tarea/AlejandroAldaco/Practica%201/img/04.png)
