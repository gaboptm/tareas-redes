#include <stdio.h>
#include <stdlib.h>
#include <strings.h>

int main( int argc, char *argv[] ) {
  int elements[argc];
  for (int i = 1; i < argc; i++ )
  {
    int c = atoi(argv[i]);
    elements[i] = c;
  }
  for (int i = 1; i <= argc; i++ )
  {
    for (int j = 1; j <= argc; j++ )
    {
      if (elements[j] > elements[j+1])
      {
        int aux = elements[j];
        elements[j] = elements[j+1];
        elements[j+1] = aux;
      }
    }
  }
  printf("Arreglo: ");
  for (int i = 1; i < argc; i++ )
  {
    printf("%s ", argv[i] );
  }
  printf("\n");
  printf("Ordenado: ");
  for (int i = 1; i <= argc; i++ )
  {
    printf("%i ",elements[i] );
  }
  printf("\n");
}
