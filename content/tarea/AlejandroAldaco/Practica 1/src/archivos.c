#include"lista.h"
#include<stdio.h>
#include<stdlib.h>
#include<dirent.h>

void imprime(void * elemento) {
    char * contenido = malloc(sizeof(char));
    contenido = elemento;
    printf("%s \n", contenido);
}

/**
 * Fución que imprime el contenido de un directorio
 * el parametro es un apuntador del nombre del directorio
 */
void leer_directorio(void * direc){
    struct dirent *dir;
    DIR *folder = opendir(direc);//apuntador de tipo directorio.
    int i = 0;
    //Mandamos error por no poder cargar el nombre del directorio.
    if(folder == NULL)
    {
        perror("Error de nombre o ruta del directorio.");
    }
    printf("Archivos :\n");
    while((dir = readdir(folder)))
    {
        i++;//índice del archivo que se lee
        printf("Archivo %d: %s\n", i ,dir->d_name );
    }
    closedir(folder);
}

/**
 * Función que dado un apuntador del nombre
 * del archivo imprime su contenido
 */
void leer_archivo(void * file){
    char ch;
    FILE *fp;
    fp = fopen(file, "r");
    //Si no existe el archivo mandamos un error.
    if (fp == NULL)
    {
        perror("Archivo sin existencia.\n");
    }
    else
    {
        printf("Contenido :\n");
        while((ch = fgetc(fp)) != EOF)
        {
            printf("%c", ch);
        }
    }
    fclose(fp);
}

/**
 * Función para insertar un archivo en la lista
 * Recibe como parámetro el apuntador sobre la lista que se insertará
 */
void insertar(struct lista *lista){
    char * element = malloc(sizeof(char));
    printf("Elemento a insertar: ");
    scanf("%s", element);
    agrega_elemento(lista, element);
    printf(">>> Elemento agregado.\n");
}

/**
 * Función para cargar el contenido del n-ésimo elemento
 * en la lista que se da como parametro.
 */
void leer(struct lista * lista){
    int n;
    printf("Índice del elemento a leer: ");
    char * archivo = malloc(sizeof(char));
    scanf("%d", &n);
    
    //Obtenemos el elemento de la lista
    archivo = obten_elemento(lista, n);
    
    DIR * folfer;
    folfer = opendir(archivo);
    //Si existe como directorio
    if (folfer != NULL)
    {
        leer_directorio(archivo);
    }
    else //en otro caso se intenta como archivo
    {
        leer_archivo(archivo);
    }
    closedir(folfer);
    printf("Fin de archivo.\n");
}

/**
 * Función que elminar el n-ésimo elemento
 * de la lista que se da como parametro
 */
void eliminar(struct lista * lista){
    int n;
    printf("Índice del elemento a eliminar: ");
    scanf("%d", &n);
    elimina_elemento(lista, n);
    printf("<<< Elmento eliminado.\n");
}

/**
 * Función que imprime los elementos de la lista
 */
void imprimir(struct lista * lista){
    printf("Elementos de la lista:\n");
    aplica_funcion(lista, imprime);
    printf(" >>> Fin de lista <<<.\n");
}

/*
 * Función principal donde se ejecuta el menu
 */
int main(){
    struct lista principal = {cabeza:0, longitud: 0};
    printf("(Ctrl+C para terminar con la ejecución)\n");
    int opcion;
    //Ciclo infinito que termina con Ctrl+C
    while(1){
        //Impresión del menú.
        printf(" *** Elegir opción *** \n\n");
        printf(" 1 Insertar archivo.\n");
        printf(" 2 Leer archivo.\n");
        printf(" 3 Eliminar archivo.\n");
        printf(" 4 Imprimir archivos.\n");
        printf(" ¿ Opción ?: ");
        scanf("%d", &opcion);//obtenemos la opción
        switch (opcion)
        {
            //Caso para ingresar un elemento.
            case 1:
            insertar(&principal);
            break;
            //Caso para leer un elemento.
            case 2:
            leer(&principal);
            break;
            //Caso para eliminar un elemento.
            case 3:
            eliminar(&principal);
            break;
            //Caso para imprimir los elementos.
            case 4:
            imprimir(&principal);
            break;
            default:
            printf("X - Verifica las opciones disponibles. - X\n");
            break;
        }
    }
    return 0;
}
