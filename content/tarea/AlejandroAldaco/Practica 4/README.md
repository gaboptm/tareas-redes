# Práctica 4: DHCP

## Alejandro Vargas aldaco

### Creación de red virtual

###### 1. Creo la red NAT

![1](img/1-red-nat.png)


###### 2. Configuro el adaptador de red en la máquina virtual

![2](img/2-adaptador-de-red.png)


###### 3. Configuro la interfaz de red en la máquina virtual

![3](img/3-configuracion-de-red.png)


###### 4. Verifico la que la ip sea la que configuré

![4](img/4-verificar-ip.png)


###### 5. Edito el arvhivo resolv.conf

![5](img/5-resolvconf.png)


###### 6. Hago ping a google

![6](img/6-ping-google.png)

### Servidor DHCP

###### 7. Instalo y Configuro el servidor DHCP

![7](img/7-servidor-dhcp.png)


###### 8. Creo otra maquina y hago la primer captura de tráfico con ambas máquinas prendidas

![8](img/8-captura-de-trafico.png)


###### 9. Compruebo que la ip de la segunda máquina está dentro del rango que configuré

![9](img/9-red-comprobacion.png)

### PXE

###### 10. Hago las modificaciones para usar PXE en el servidor DHCP

![10](img/10-pxe-modificaciones.png)


###### 11. Instalo y configuro el servidor tftp y descargo el instalador por red de Debian

![11](img/11-pxe-tftp.png)


###### 12. Creo la nueva máquina virtual que se conectara a la red y listo las máquinas

![12](img/12-pxe-lista-vms.png)


###### 13. Prendo la nueva máquina y sale el instalador de Debian (Aclaro que para este paso se tuvo que actualizar virtualbox, por eso difiere su interfaz)

![13](img/13-pxe-instalador-debian.png)


###### 14. Muestro el syslog del servidor Debian

![14](img/13-pxe-syslog.png)


###### 15. Antes de encender la máquina que recibe el instalador por PXE, hice una captura de tráfico y en este paso muestro los resultados  del trafico tftp (output2.pcap)

![15](img/14-pxe-captura-de-trafico.png)



## Respuesta a las siguientes preguntas:

* ¿Cómo funciona el protocolo DHCP?
  R: La máquina que se conecta a la red hace un broadcast en busca de un servidor dhcp y el servidor dhcp le responde con la asignación de un ip.
* ¿Cómo se filtra el tráfico del protocolo DHCP en una captura de tráfico en Wireshark y en tcpdump?
  R: Defino en1 como mi interfaz de red y verifico el puerto por donde están pasando las conexiones del protocolo dhcp, comunmente son los puertos 67 o 68
  $ tcpdump -i en1 -n port 67 and port 68
* ¿Qué es el estándar PXE y cuál es su utilidad?
  R: Preboot eXecution Environment, permite el arranque de un software que se recibe por red en una computadora donde no está iniciado aún el sistema operativo.
  Su utilidad más común es el permitir a los administradores de sistemas tener repositorios de software y hacer la instalación del S.O. desde una imagen de red configurada previamente
