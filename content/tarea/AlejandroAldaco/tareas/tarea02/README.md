# Tarea TCP

### Alejandro Vargas Aldaco

###### 1. Verificar que el dominio tenga el servicio de HTTP
![1](https://gitlab.com/alex.aldaco/tareas-redes/raw/master/content/tarea/AlejandroAldaco/tareas/tarea02/img/01.png)

###### 2. Captura de tráfico
```bash tcpdump -n -w www_khronos_com.pcap 'host www.khronos.com' ```

###### 3. Petición HTTP
![2](https://gitlab.com/alex.aldaco/tareas-redes/raw/master/content/tarea/AlejandroAldaco/tareas/tarea02/img/02.png)

###### 4. Cerrar la captura de tráfico y visualizar el resultado

![5](https://gitlab.com/alex.aldaco/tareas-redes/raw/master/content/tarea/AlejandroAldaco/tareas/tarea02/img/05.png)

![3](https://gitlab.com/alex.aldaco/tareas-redes/raw/master/content/tarea/AlejandroAldaco/tareas/tarea02/img/03.png)


![4](https://gitlab.com/alex.aldaco/tareas-redes/raw/master/content/tarea/AlejandroAldaco/tareas/tarea02/img/04.png)