#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <pcap.h>

int main(int argc, char *argv[])
{
    const char *interfaz = argv[1];
    char *token;
    const char delimiter[2] = ":";
    char error[PCAP_ERRBUF_SIZE];
    char payload[17] = "My custom payload";
    u_char packet[100];
    pcap_t *handle;
    int i = 0;
    if (argc != 4)
    {
        printf("Uso: \tcommand \t<interfaz> \t<MAC origen>      \t<MAC destino>\n");
        printf("     \t./inyector      \teth0       \tXX:XX:XX:XX:XX:XX \tYY:YY:YY:YY:YY:YY \n");
        exit(1);
    }
    error[0] = '\0';
    handle = pcap_open_live(interfaz, 100, 0, 100,error);
    if (error[0] != '\0')
    {
        perror(error); return 0;
    }
    token = strtok(argv[2], delimiter);
    while(token != NULL)
    {
        packet[i++] = strtol(token, NULL, 16);
        token = strtok(NULL, delimiter);
    }
    token = strtok(argv[3], delimiter);
    while(token != NULL)
    {
        packet[i++] = strtol(token, NULL, 16);
        token = strtok(NULL, delimiter);
    }
    memcpy(packet + sizeof(payload) + 1, payload, sizeof(payload) + 1);
    if (pcap_inject(handle, packet, 100) == -1)
    {
        perror(pcap_geterr(handle));
    }

    return 0;
}

