# Práctica 5
- Rivera González Damian
- Vargas Aldaco Alejandro

## 1. Inyección de tramas

Para este ejercicio utilizamos un equipo mac y una maquina virtual con debian instalada en elquipo mac con virtualbox, y una interfaz de red en debian conectada en modo puente al mismo router al que se conecta el equipo mac. 

Primero vemos la dirección mac de la interfaz que vamos a usar para enviar la trama

```bash
    $ ifconfig
    ...
    en1: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	ether 20:c9:d0:b9:34:f7
    inet6 fe80::424:8ca3:d7dd:5ede%en1 prefixlen 64 secured scopeid 0x8 
	inet 192.168.0.10 netmask 0xffffff00 broadcast 192.168.0.255 
    ...
```

Seguido vemos la interfaz y su dirección mac del equipo remoto

```bash
    $sudo ifconfig
    enp0s8: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.0.13  netmask 255.255.255.0  broadcast 192.168.0.255
        inet6 fe80::a00:27ff:fe8f:f486  prefixlen 64  scopeid 0x20<link>
        ether 08:00:27:8f:f4:86  txqueuelen 1000  (Ethernet)
```

Ya que sabemos la dirección mac y la interfaz en ambos sistemas, envíamos la trama de la siguiente manera 

```bash 
    $ ./inyector en1 08:00:27:8f:f4:86 20:c9:d0:b9:34:f7 
```
Donde podemos notar que:
    - en1 es la interfaz del equipo donde se envía la trama
    - 08:00:27:8f:f4:86 es la mac address destino
    - 20:c9:d0:b9:34:f7 es la mac address de origen

Previamente, hacemos la captura en el equipo que recibirá la trama con tcpdump

```bash
    $ sudo tcpdump -i enp0s8 -XX ether host 20:c9:d0:b9:34:f7 -w trama.pcap
```

Paramos la captura de tráfico y leemos el resultado

```bash 
    $ sudo tcpdump -Aennvv -r trama.pcap 
    ...
    reading from file trama.pcap, link-type EN10MB (Ethernet)
    02:58:13.830519 20:c9:d0:b9:34:f7 > 08:00:27:8f:f4:86, 802.3, length 0: LLC, dsap Null (0x00) Individual, ssap Null (0x00) Command, ctrl 0x0000: Information, send seq 0, rcv seq 0, Flags [Command], length 86
        0x0000:  0000 0000 4d79 2063 7573 746f 6d20 7061  ....My.custom.pa
        0x0010:  796c 6f61 6400 0000 0000 0000 0000 0000  yload...........
        0x0020:  0000 0000 0000 0000 0000 0000 0000 0000  ................
        0x0030:  0000 0000 0000 0000 0000 0000 0000 0000  ................
        0x0040:  0000 0000 0000 0000 0000 0000 0000 0000  ................
        0x0050:  0000 0000 0000                           ......
    My custom payload.................................................................
    ...
```

![trama](img/trama.png)

## 2 DNS

#### Configuración para el dominio **aldaco.cf**

Adquiero el dominio en freenom.com

![201](img/domain.png)

Agrego el dominio en dns.he.net

![202](img/add-domain.png)

Configuro el dominio para que apunte a la ip local 127.0.0.1

![203](img/add-a-record.png)

Verifico los registros dns (la advertencia se quita al final cuando presiono delegation check)

![204](img/dns-records.png)

Configuro los ns servers

![205](img/configure-ns-servers.png)

#### Verifico con nslookup

![206](img/nslookup.png)


#### Preguntas

    - ¿Qué es un servidor DNS?

El Sistema de Nombres de Dominio o DNS es un sistema de nomenclatura jerárquico que se ocupa de la administración del espacio de nombres de dominio (Domain Name Space). Su labor primordial consiste en resolver las peticiones de asignación de nombres. Para ello, el sistema de nombres de dominio recurre a una red global de servidores DNS, que subdividen el espacio de nombres en zonas administradas de forma independiente las unas de las otras. Esto permite la gestión descentralizada de la información de los dominios.

Cuando se introduce la dirección de una página web (URL) en el campo de búsqueda del navegador, este realiza una petición al llamado resolver, un componente especial del sistema operativo cuya función consiste en almacenar en caché direcciones IP ya solicitadas anteriormente, y proporcionarlas cuando la aplicación cliente (navegador, programa de correo) la solicita. Si la dirección IP solicitada no se encuentra en el caché del resolver, este redirige la petición al servidor DNS que corresponda, que, en general, se trata del servidor DNS del proveedor de Internet. Aquí se coteja la petición con la base de datos del DNS y, si está disponible, se envía la dirección IP correspondiente como respuesta (“forward lookup”). Esta permite al navegador del usuario dirigirse al servidor web deseado en Internet. Otra vía alternativa consiste en el camino inverso, es decir, en traducir la dirección IP en la dirección de dominio (“reverse lookup”).

    - ¿Para qué se utiliza cada uno de los registros SOA, NS, A, AAAA, PTR, CNAME, MX y TXT?

**SOA** Un registro "SOA" significa "Comienzo de autoridad". Evidentemente, es uno de los registros DNS más importantes, dado que guarda información esencial, como la fecha de la última actualización del dominio y otros cambios y actividades.

**NS** significa Servidor de nombres e indica qué nombre del servidor es el autorizado para el dominio.
**A** El registro "A" hace referencia a la Dirección y es el tipo más básico de sintaxis de DNS.  Indica la dirección de IP real de un dominio.

**AAA** El registro "AAAA" (también conocida como dirección IPV6) indica el nombre de alojamiento a una dirección IPv6 de 128 bits.  Las direcciones DNS normales se mapean para direcciones IPv4 de 32 bits.

**PTR** El registro "PTR" significa "Punto de terminación de red". La sintaxis de DNS es responsable del mapeo de una dirección Ipv4 para el CNAME en el alojamiento.

**CNAME** El registro de "CNAME" significa nombre canónico y su función es hacer que un dominio sea un alias para otro. El CNAME generalmente se utiliza para asociar nuevos subdominios con dominios ya existentes de registro A.

**MX** El registro "MX" o intercambio de correo es principalmente una lista de servidor de intercambio de correo que se debe utilizar para el dominio.

**TXT** Un registro "TXT" significa "Texto". Esta sintaxis de DNS permite que los administradores inserten texto en el registro DNS. A menudo se utiliza para denotar hechos o información sobre el dominio. **También sirve para ingresar los registros spf y dkim para la validación de los correos electrónicos**

    - ¿Qué hacen las herramientas nslookup y dig?

**nslookup** es una herramienta para saber si el DNS está resolviendo correctamente los nombres y las IPs. Se utiliza con el comando nslookup, que funciona tanto en Windows como en UNIX para obtener la dirección IP conociendo el nombre, y viceversa.

**dig** es una herramienta para realizar consultas a los servidores DNS para solicitar información sobre direcciones de host, intercambiadores de correo, servidores de nombres e información relacionada.


## Packet Tracer
#### Creación de redes.
#### Red 192.168.1.0/24

Creamos la primera red.
Un switch y dos computadoras.

![](img/02pt.png)

Configuramos las IP de las dos computadoras.

![](img/03pt.png)

![](img/04pt.png)

Verificamos que ambas computadoras pueden comunicarse.

![](img/05pt.png)

#### Red 10.10.10.0/24

Conectamos una PC, una impresora y un servidor a un switch.

![](img/06pt.png)

Asignamos una dirección IP estática al Servidor DHCP

![](img/07pt.png)

Configuramos el servidor DHCP en la pestaña `Services`, desactivando las opciones HTTP y HTTPS

![](img/08pt.png)

Vamos a la seccion DHCP, habilitamor el servicio DHCP, asignamos un gateway, un servidor DNS, una dirección IP inicial y una
máscara de Red, y guardamos la configuración.

![](img/09pt.png)

Indicamos que se le asigne una dirección IP de manera dinámica a la computadora (PC2) conectada al servidor, a través de DHCP).

![](img/10pt.png)

De igual manera con la impresora.

![](img/11pt.png)

Verificamos que la computadora (PC2) se pueda comunicar con la impresora (printer0).

![](img/12pt.png)


#### Red 189.240.70.0/24

Conectamos dos servidores a un switch.

![](img/13pt.png)

Asignamos una dirección IP a. servidor web `www.redes.net`

![](img/14pt.png)

Asignamos una dirección IP al servidor DNS.

![](img/15pt.png)

Verificamos que se puedan comunicar entre ambos servidores.

![](img/16pt.png)

Habilitamos el servico de HTTP y HTTPS en `www.redes.net`

![](img/17pt.png)

Habilitamos el servicio DNS en el servidor DNS y creamos un registro de tipo A con los datos del servidor web:

![](img/18pt.png)


#### Conexión de Redes.

Conectamos las redes `192.168.1.0/24` y `10.10.10.0/24` con la red `189.240.70.0/24` a través de routers. Creamos tres
routers y los conectamos entre ellas.

![](img/19pt.png)

Configuramos los routers de la siguiente manera.
Apagar router haciendo click en el botón de encendido

Agregamos una tarjeta de red de tipo `HWIC-2T`, arrastrándola a uno de los espacios vacios en el router, esta tarjeta permite
transmitir datos a través de una `WAN` por medio de puertos seriales. Y luego de ello prendemos el router.

![](img/20pt.png)

Ingresar a la pestaña `CLI` para ingresar a la línea de comandos, entrar a modo privilegiado:

```
Router>enable
Router#
```

Entrar a la configuración global

```
Router#configure terminal
```

Cambiar _hostname_ de router

```
Router(config)#hostname Router0
Router0(config)#
```

Asignar una contraseña para entrar a modo privilegiado

```
Router0(config)#enable secret <contraseña>
```

Cada vez que se quiera entrar a modo privilegiado con `enable` se pedirá esta contraseña. Para guardar configuración, salir de configuración global y copiar la configuración actual a la configuración de inicialización:

```
Router0(config)#exit
Router0#copy running-config startup-config
```

Conectar los routers con cables de tipo `Serial DTE` por medio de los puertos seriales de la siguiente manera:

![](img/21pt.png)

Asignar direcciones IP a interfaces de routers y prenderlas, esto puede hacerse por medio de interfaz gráfica o línea de comandos, se mostrará la configuración de Router0 por línea de comandos:

```
Router0>enable
Password:
Router0#configure terminal
Enter configuration commands, one per line.  End with CNTL/Z.
Router0(config)#interface FastEthernet0/0
Router0(config-if)#ip address 189.240.70.1 255.255.255.0
Router0(config-if)#no shutdown

Router0(config-if)#
%LINK-5-CHANGED: Interface FastEthernet0/0, changed state to up

%LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0, changed state to up

Router0(config-if)#exit
Router0(config)#interface serial0/1/0
Router0(config-if)#ip address 189.240.220.1 255.255.255.252
Router0(config-if)#no shutdown

%LINK-5-CHANGED: Interface Serial0/1/1, changed state to down
Router0(config-if)#exit
Router0(config)#interface serial0/1/1
Router0(config-if)#ip address 189.240.230.1 255.255.255.252
Router0(config-if)#no shutdown
Router0(config-if)#exit
Router0(config)#exit
Router0#
%SYS-5-CONFIG_I: Configured from console by console

Router0#copy running-config startup-config
Destination filename [startup-config]?
Building configuration...
[OK]
Router0#
```

Se utiliza el comando `interface <interfaz>` para acceder a la configuración de una interfaz, el comando `ip address <ip> <mascara>` para asignar una dirección IP a la interfaz y el comando `no shutdown` para prender la interfaz.

Verificar configuración de router:

`Router0#show running-config`.

Crear rutas estáticas en Router1 y Router2 para comunicar las redes. Configurar Router1 de la siguiente manera:

```
Router1>enable
Password:
Router1#configure terminal
Router1(config)#ip route 189.240.70.0 255.255.255.0 serial0/1/0
```

 El comando `ip route 189.240.70.0 255.255.255.0 serial0/1/0` indica que se envíe el tráfico dirigido a la red 189.240.70.0/24 por la interfaz serial0/1/0.

Repetimos para Router2.

#### Configuración de NAT/PAT

Configurar PAT en Router1:

```
Router1#configure terminal
Router1(config)#access-list 1 permit 192.168.1.0 0.0.0.255
Router1(config)#ip nat inside source list 1 int serial0/1/0 overload
Router1(config)#interface serial0/1/0
Router1(config-if)#ip nat outside
Router1(config-if)#exit
Router1(config)#interface fa0/0
Router1(config-if)#ip nat inside
Router1(config-if)#exit
Router1(config)#exit
Router1#copy running-config startup-config
```

El comando `access-list 1 permit 192.168.1.0 0.0.0.255` crear una lista de acceso con la cual se permite el tráfico de la red 192.168.1.0/24.

`ip nat inside source list 1 int serial0/1/0 overload` indica que las direcciones IP del tráfico permitido por la lista de acceso creada (la que tiene el identificador 1) sean traducidas por medio de PAT a la dirección IP asignada a la interfaz serial0/1/0.

`ip nat outside` indica que la interfaz se conecta al tráfico externo de NAT.

`ip nat inside` indica que la interfaz se conecta al tráfico interno de NAT.

Repetir lo anterior para Router2, cambiando la red de la lista de acceso.

Verificar que desde el navegador web de PC0 se pueda acceder a `www.redes.net`:

![](img/22pt.png)

Verificamos lo mismo para PC2:

![](img/23pt.png)

Para ver las estadísticas de NAT, usar el siguiente comando:

```
Router>show ip nat statistics
Total translations: 4 (0 static, 4 dynamic, 4 extended)
Outside Interfaces: Serial0/1/0
Inside Interfaces: FastEthernet0/0
Hits: 54  Misses: 8
Expired translations: 4
Dynamic mappings:
```
También se puede observar la tabla de traducciones:

```
Router>show ip nat translations
Pro  Inside global     Inside local       Outside local      Outside global
tcp 189.240.220.2:1025 192.168.1.2:1025   189.240.70.2:80    189.240.70.2:80
tcp 189.240.220.2:1026 192.168.1.2:1026   189.240.70.2:80    189.240.70.2:80
tcp 189.240.220.2:1027 192.168.1.2:1027   189.240.70.2:80    189.240.70.2:80
tcp 189.240.220.2:1028 192.168.1.2:1028   189.240.70.2:80    189.240.70.2:80
```

#### Preguntas
+ ¿Qué diferencia hay entre un switch y un router?
El switch envía los paquetes dentro de la red LAN, opera en la capa de enlace.
Los routers son capaces de escoger la mejor ruta actual para enviar paquetes de un origen al destino.
Los routers pueden interconectar varias redes.

+ ¿Qué tipos de switches hay?
LAN Switch
Unmanaged Network Switches
Managed Switches
PoE Switch
Stackable Switch

+ ¿Qué es una lista de acceso y para qué se utilizan?

Permiten controlar el flujo del tráfico en equipos de redes, tales como enrutadores y conmutadores. Su principal objetivo es
filtrar tráfico, permitiendo o denegando el tráfico de red de acuerdo a alguna condición

+ Se creó una lista de acceso con el comando `access-list 1 permit 192.168.1.0 0.0.0.255`, la última parte del comando
(0.0.0.255) es una _wildcard_, ¿qué es una wildcard y en qué se diferencia con una máscara de red?
Es una máscara de subred, con una regla de correspondencia:
El 0 significa que se debe comprobar el bit equivalente
El 1 significa que el bit equivalente no importa.
Las máscaras wildcard son utilizadas en situaciones donde las máscaras de subred no pueden aplicar.

+ ¿Qué diferencia hay entre una dirección IP pública y una privada?

Una IP pública sirve para hacer la identificación de un dispositivos a nivel de diferentes redes. Mientras que una IP privada
es una dirección que sirve para identificar a un dispositivo dentro de una red.

+ ¿Qué diferencia hay entre NAT estática, NAT dinámica y PAT? ¿Cuál es la que se encuentra usualmente en los routers de los
hogares?

NAT Estático
Consiste básicamente en un tipo de NAT en el cuál se mapea una dirección IP privada con una dirección IP pública de forma
estática. De esta manera, cada equipo en la red privada debe tener su correspondiente IP pública asignada para poder acceder
a Internet. La principal desventaja de este esquema es que por cada equipo que se desee tenga acceso a Internet se debe
contratar una IP pública.

NAT Dinámico
Este tipo de NAT pretende mejorar varios aspectos del NAT estático dado que utiliza un pool de IPs públicas para
un pool de IPs privadas que serán mapeadas de forma dinámica y a demanda. La ventaja de este esquema es que si se tienen por
ejemplo 5 IPs públicas y 10 máquinas en la red privada, las primeras 5 máquinas en conectarse tendrán acceso a Internet.

PAT (Por Address Translation)
Es una característica del estándar NAT, que traduce conexiones TCP y UDP hechas por un host y un puerto en una red externa a
otra dirección y puerto de la red interna.
