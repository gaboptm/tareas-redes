# Práctica 6: Web Stack

- Cortez Flores Luis Enrique
- Rivera González Damian
- Vargas Aldaco Alejandro

--------
### Instalación de Drupal 8 en un entorno LAMP

#### 1. Instalación de paquetes

**apache2 mariadb-server php-fpm memcached php-memcached php-dom php-gd php-xml php-mbstring**

![01](img/01.png)

mariadb-server y memcached los dejamos con la configuración por defecto porque esa configuración es funcional para esta instalación

    Si se quisiera tunear mariadb-server se edita alguno de estos archivos
        - /etc/mysql/my.cnf
        - /etc/mysql/conf.d/*
        - /etc/mysql/mariadb.conf.d/*

    Para el caso de memcached, son los siguientes archivos
        - /etc/memcached.conf
        - /etc/php/7.0/fpm/conf.d/25-memcached.ini

#### 2. Configuración de php7.0-fpm

```bash
    $ sudo nano /etc/php/7.0/fpm/php.ini
    ...
    date.timezone = America/Mexico_City
    memory_limit = 256M
    opcache.enable=1
    opcache.save_comments=1
    ...
```

Se reinicia php-fpm y checamos la configuración

![02](img/02.png)

Integramos php-fpm con apache2

```bash
    $ sudo a2enmod proxy_fcgi setenvif
    $ sudo a2enconf php7.0-fpm
    $ sudo systemctl reload apache2.service
```

Verificamos que apache no tenga errores después de la integración

![03](img/03.png)

Ahora verificamos que apache2 esté funcionando con php-fpm.

Creamos un archivo info.php

```bash
    $ sudo nano /var/www/html/info.php
    ...
    <?php phpinfo(); ?>
    ...
```

Y verificamos el funcionamiento en el navegador

![04](img/04.png)


#### 3. Creación de la base de datos

```bash
    $ sudo mysql -p
    MariaDB [(none)]> create database drupal;
    MariaDB [(none)]> create user drupaluser@localhost identified by 'drupalpassword';
    MariaDB [(none)]> grant all privileges on drupal.* to drupaluser@localhost;
    MariaDB [(none)]> flush privileges;
    MariaDB [(none)]> exit;
```

#### 4. Instalación de Drupal 8 en el servidor

Nos posicionamos en el lugar donde se ubican los sitios por defecto

```bash
    $ cd /var/www/
```

Descargamos y extraemos Drupal 8

```bash
    $ sudo wget https://www.drupal.org/download-latest/tar.gz
    $ sudo tar zxvf tar.gz
    $ sudo rm -R html
    $ sudo mv drupal-8.6.14/ html
    $ sudo chown -R www-data. html/
    $ ls -la
        total 16752
        drwxr-xr-x  4 root     root         4096 Apr 16 00:42 .
        drwxr-xr-x 12 root     root         4096 Apr 15 23:39 ..
        drwxr-xr-x  8 www-data www-data     4096 Apr  7 20:53 html
        -rw-r--r--  1 root     root     17136296 Apr  7 20:53 tar.gz
```

Preparamos apache2 para la instalación (agregamos las directivas que mencionamos en el siguiente código al virtualhost)

```bash
    $ sudo nano /etc/apache2/sites-enabled/000-default.conf
    ...
        <IfModule mod_rewrite.c>
           RewriteEngine On
        </IfModule>

        <Directory /var/www/html/>
           Options Indexes FollowSymLinks
           AllowOverride All
           Require all granted
        </Directory>
    ...
    $ sudo systemctl restart apache2.service
```



Abrimos el instalador de Drupal 8 en el navegador

![05](img/05.png)

Escogemos la instalación de demostración

![06](img/06.png)

Configuramos la DB en Drupal 8 y le damos a instalar

![07](img/07.png)

Una vez instalado, configuramos el sitio

![08](img/08.png)

Y ahora Drupal 8 ha sido instalado en el servidor

![09](img/09.png)

#### 5. Integración de Drupal 8 y memcached

Verificamos el funcionamiento de memcached (ok)

![10](img/10.png)

Instalamos los módulos necesarios

![11](img/11.png)

Editamos el archivo settings.php para que Drupal 8 trabaje con memcached

```bash
    $ sudo nano /var/www/html/sites/default/settings.php
    ...
    $settings['cache']['default'] = 'cache.backend.memcache_storage';
    $settings['memcache_storage']['key_prefix'] = 'RED';
    $settings['memcache_storage']['memcached_servers'] =  ['127.0.0.1:11211' => 'default'];
    ...
```

Verificamos que la integración funcione (ok)

![12](img/12.png)

También en la terminal

![13](img/13.png)

#### 6. Configuración del protocolo ssl en apache2

Agregamos el módulo ssl en apache2 y el módulo headers para brindar mayor seguridad

```bash
    $ sudo a2enmod ssl headers http2
```

Creamos el certificadl ssl autofirmado

```bash
    $ cd /etc/ssl/
    $ sudo openssl req -x509 -nodes -days 9999 -newkey rsa:2048 -keyout private/red.key -out certs/red.crt -subj "/CN=red"
```

Configuramos el virtualhost de apache2 que funcionara con ssl (Agregamos el soporte para http2 para una mejora en el performace de respuesta del servidor)

```bash
    $ cd /etc/apache2
    $ sudo nano sites-available/default-ssl.conf
    ...
                Protocols h2 h2c http/1.1
                <Directory /var/www/html/>
                        Options -MultiViews +FollowSymLinks
                        AllowOverride All
                        Require all granted
                </Directory>

                <IfModule mod_rewrite.c>
                        RewriteEngine On
                </IfModule>

                <IfModule mod_headers.c>
                  Header append Vary User-Agent
                  Header append Vary Accept-Encoding
                  Header set Strict-Transport-Security "max-age=31536000; includeSubDomains; preload"
                  Header set X-XSS-Protection "1; mode=block"
                  Header set X-Content-Type-Options nosniff
                  Header always append X-Frame-Options DENY
                </IfModule>
    ...
                SSLCertificateFile      /etc/ssl/certs/red.crt
                SSLCertificateKeyFile /etc/ssl/private/red.key
    ...
                BrowserMatch "MSIE [2-6]" \
                                nokeepalive ssl-unclean-shutdown \
                                downgrade-1.0 force-response-1.0
                # MSIE 7 and newer should be able to use keepalive
                BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown
                SSLProtocol all -SSLv2 -SSLv3
                SSLHonorCipherOrder on
                SSLCipherSuite "EECDH+ECDSA+AESGCM EECDH+aRSA+AESGCM EECDH+ECDSA+SHA384 EECDH+ECDSA+SHA256 EECDH+aRSA+SHA384 EECDH$
                Header always set Strict-Transport-Security "max-age=63072000; includeSubdomains;"
    ...
```

Activamos el virtualhost para ssl

```bash
    $ sudo a2ensite default-ssl.conf
    $ sudo systemctl restart apache2.service
```

Verificamos que funcione la conexión por medio de ssl (Para este paso se habilitó una nueva interfaz de red que se conecta directamente al router en modo puente, ya que la conexión localhost por medio de red nat falló aun con el reenvío de puertos en virtualbox)

![14](img/14.png)

Hasta este paso aún se puede acceder al sitio con o sin ssl

![15](img/15.png)

#### 7. Configuración de las reglas de iptables

Instalamos netfilter-persistent para guardar las reglas tras un reinicio del servidor y agregamos las reglas de iptables

```bash
    $ sudo apt install iptables-persistent netfilter-persistent
    $ sudo iptables -A INPUT -i lo -j ACCEPT
    $ sudo iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
    $ sudo iptables -A INPUT -p icmp -m icmp --icmp-type any -j ACCEPT
    $ sudo iptables -A INPUT -p tcp -m tcp --dport 80 -j ACCEPT
    $ sudo iptables -A INPUT -p tcp -m tcp --dport 443 -j ACCEPT
    $ sudo iptables -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
    $ sudo iptables -A INPUT -p udp -m udp --sport 53 -j ACCEPT
    $ sudo iptables P INPUT DROP
```

Verificamos en la terminal las reglas del firewall

![16](img/16.png)

Guardamos las reglas y verificamos el archivo rules.v4

![17](img/17.png)

### Captura de tráfico

Para la captura de tráfico en primera parte se desactivó la redirección 301 de http a https y se capturo el tráfico desde http y desde https. Seguido se activo la redirección y se continuó con la captura de tráfico

```bash
    $  sudo tcpdump -Aenn -r output.pcap
```

Se hizo un inicio de sesión en el sitio en http y https. En el caso de http pudimos obtener los datos del inicio de sesión

```bash
    $ sudo tcpdump -A -r output.pcap | grep 'pass='
    ...
    reading from file output.pcap, link-type EN10MB (Ethernet)
    name=Admin&pass=admin&form_build_id=form-VcUvsWAj86Si1WlM5dC9-Ah5MJgMipVxfvks5HBfclY&form_id=user_login_form&op=Iniciar+sesi%C3%B3n
    ...
```
En la captura también se puede apreciar la redirección 301 de http a https (para el punto extra)

```bash
    $ sudo tcpdump -A -r output.pcap | grep '301 Moved'
    ...
    reading from file output.pcap, link-type EN10MB (Ethernet)
    15:31:37.158764 IP 192.168.0.13.http > 192.168.0.7.58224: Flags [P.], seq 1:580, ack 571, win 236, options [nop,nop,TS val 2490346 ecr 332050333], length 579: HTTP: HTTP/1.1 301 Moved Permanently
    .%......HTTP/1.1 301 Moved Permanently
    <title>301 Moved Permanently</title>
    ...
```

**El archivo de captura es output.pcap**

### Puntos extra

#### Permitir que los usuarios accedan únicamente por https

Configuramos el virtualhost que maneja http y agregamos unas nuevas directivas en mod_rewrite y con eso se activa https en automático para las peticiones http

```bash
    $ sudo nano /etc/apache2/sites-enabled/000-default.conf
    ...
        <IfModule mod_rewrite.c>
           RewriteEngine On
           RewriteCond %{HTTPS} !on
           RewriteRule .* https://%{HTTP_HOST}/%{REQUEST_URI} [R=301,L,QSA]
           RewriteCond %{HTTP_USER_AGENT} libwww-perl.*
           RewriteRule .* ? [F,L]
        </IfModule>
    ...
```

#### Instalar sitios de administración para los servicios (phpMyAdmin, phpMemcachedAdmin)

- phpPgAdmin no se va a instalar porque se utilizó mariadb en lugar de postgresql
- phpRedisAdmin no se va a instalas porque se utilizó memcahed en lugar de redis (son similares)

##### Instalación de phpMyAdmin

```bash
    $ sudo apt install phpmyadmin
```

Seguimos las instrucciones

![18](img/18.png)

Verificamos que funcione en el navegador y entramos con el usuario de la BD del sitio

![19](img/19.png)

##### Instalación de phpMemcachedAdmin (A pesar de que en Drupal 8 se instaló una interfaz de administración de memcached, lo haremos con phpMemcachedAdmin)


```bash
    $ cd /var/www/
    $ sudo mkdir phpmemcachedadmin
    $ sudo wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/phpmemcacheadmin/phpMemcachedAdmin-1.2.2-r262.tar.gz
    $ sudo tar zxvf phpMemcachedAdmin-1.2.2-r262.tar.gz -C phpmemcachedadmin/
    $ sudo nano /etc/apache2/conf-available/phpmemcachedadmin.conf
    ...
        # phpmemcachedadmin default Apache configuration
        Alias /phpmemcachedadmin /var/www/phpmemcachedadmin
        <Directory /var/www/phpmemcachedadmin>
            Options SymLinksIfOwnerMatch
            DirectoryIndex index.php
            Require all granted
            <IfModule mod_php5.c>
                <IfModule mod_mime.c>
                    AddType application/x-httpd-php .php
                </IfModule>
                <FilesMatch ".+\.php$">
                    SetHandler application/x-httpd-php
                </FilesMatch>
            </IfModule>
            <IfModule mod_php.c>
                <IfModule mod_mime.c>
                    AddType application/x-httpd-php .php
                </IfModule>
                <FilesMatch ".+\.php$">
                    SetHandler application/x-httpd-php
                </FilesMatch>
            </IfModule>
        </Directory>
    ...
    $ sudo a2enconf phpmemcachedadmin.conf
    $ sudo systemctl restart apache2.service
```

Verificamos la instalación en el navegador

![20](img/20.png)

##### Instalación de php desde otro repositorio

**Debian 9 viene con php7.0. Instalaremos php7.3 desde el otro repositorio**

```bash
    $ cd
    $ sudo apt-get -y install apt-transport-https lsb-release ca-certificates
    $ sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
    $ sudo sh -c 'echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list'
    $ sudo apt update && sudo apt -y upgrade
    $ sudo apt autoremove
```

verificamos la nueva version

```bash
    $ php -v
    ...
    PHP 7.3.4-1+0~20190412071350.37+stretch~1.gbpabc171 (cli) (built: Apr 12 2019 07:13:50) ( NTS )
    Copyright (c) 1997-2018 The PHP Group
    Zend Engine v3.3.4, Copyright (c) 1998-2018 Zend Technologies
    with Zend OPcache v7.3.4-1+0~20190412071350.37+stretch~1.gbpabc171, Copyright (c) 1999-2018, by Zend Technologies
    ...
```

Ahora para que funcione php7.3-fpm hay que hacer lo siguiente

```bash
    $ sudo a2disconf php7.0-fpm.conf
    $ sudo a2enconf php7.3-fpm.conf
    $ sudo a2dismod php7.3
    $ sudo systemctl restart apache2
```
    $ sudo a2dismod php7.3: lo desactivamos porque php7.3-fpm funciona como proceso separado y se utiliza por medio de un proxy

Configuramos ahora php7.3-fpm con la configuración que ya teniamos con php7.0-fpm

```bash
    $ sudo nano /etc/php/7.3/fpm/php.ini
    ...
    date.timezone = America/Mexico_City
    memory_limit = 256M
    opcache.enable=1
    opcache.save_comments=1
    ...
    $ sudo systemctl restart php7.3-fpm.service
```

##### Instalación de MariaDB desde el repositorio oficial

**Debian 9 viene con la versión 10.1. Instalaremos MariaDB 10.3 desde el repositorio oficial**

```bash
    $ sudo apt-get install software-properties-common dirmngr
    $ sudo apt-key adv --recv-keys --keyserver keyserver.ubuntu.com 0xF1656F24C74CD1D8
    $ sudo add-apt-repository 'deb [arch=amd64,i386,ppc64el] http://mirror.its.dal.ca/mariadb/repo/10.3/debian stretch main'
    $ sudo apt-get update && sudo apt -y upgrade
    $ sudo apt-get install mariadb-server
```

    $ sudo apt-get install mariadb-server // Se vuelve a instalar porque el upgrade no actualizó a la nueva versión

Verificamos que la instalación haya sido exitosa y que el sitio funcione con los nuevos paquetes

![21](img/21.png)
