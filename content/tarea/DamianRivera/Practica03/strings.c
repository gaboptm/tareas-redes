#include <stdio.h>
#include <string.h>

/**
 * Función que dada una cadena regresa su valor en decimal.
 * @param  hexVal[]: cadena en forma de hexadecimal
 * @retval el valor en decimal de la cadena.
 */
int hexadecimalToDecimal(char hexVal[]) 
{    
    int len = strlen(hexVal); //Tamño de la cadena en hexa
    
    //Valor de la base inicializado en 1, i.e 16^0 
    int base = 1; 
    int dec_val = 0;//valor en decimal a regresar 
      
    // Obtenemos el valor del último caracter
    for (int i=len-1; i>=0; i--) 
    {    
        //Si es un valor númerico calculamos su valor decimal.
        if (hexVal[i]>='0' && hexVal[i]<='9') 
        { 
            dec_val += (hexVal[i] - 48)*base; 
            base = base * 16; //indrementamos el valor de la base
        } 

        //Si es una letra mayúcula calculamos su valor en decimal.
        else if (hexVal[i]>='A' && hexVal[i]<='F') 
        { 
            dec_val += (hexVal[i] - 55)*base; 
            base = base*16; //incrementamos el valor de la base
        } 
        //si es una letra minúcula calculamos su valor en decimal.
        else if (hexVal[i]>='a' && hexVal[i]<='f') 
        { 
            dec_val += (hexVal[i] - 87)*base; //sumamos el valor al decimal actual
            base = base*16; //incrementamos el valor de la base.
        } 
    } 
    return dec_val; //regresamos la suma total del valor decimal.
}


int main(int argc, char **argv) { 
    FILE *fp;//apuntador para abrir el archivo.
    char str[128];//para trabajar línea por línea
    char *token;//para obtener el valor del código en hexadecimal
    int imprimio = 0;
    if (argc < 2 || argc > 2) {
        printf("Falta de parametros\n Uso: ./Ej2 <archivo> \n Ejemplo: ./Ej2 archivo\n");
        return -1;
    }    
    
    fp = fopen(argv[1],"rb");
    //verificamos que el archivo existe.
    if(fp == NULL) {
        perror("Error al abrir el archivo.");
        return(-1);
    }
    else
    {   
        while(fgets(str, 128, fp)!= NULL){//leemos línea por línea.
            token = strtok(str, " ");//obtenemos el token separado por espacios.

            while(token != NULL){
                int imprimible = hexadecimalToDecimal(token);
                //verificamos que el token este dentro del rango imprimible de ASCII
                if (imprimible >= 32 && imprimible <= 126 ) {
                    printf("%c", imprimible);
                    imprimio = 1;
                }else if(imprimio ==1)//ponemos un salto luego de imprimir un caracter válido.
                {
                    printf("\n");
                    imprimio = 0;
                }
                token = strtok(NULL, " ");//actualizamos el token

            }//while de tokens
        }//while de lineas
    }
    fclose(fp);
   return 0;
      
}