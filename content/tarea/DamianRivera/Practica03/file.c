#include <stdio.h>

int main(int argc, char const *argv[])
{

   int n_archivos = 1;//variable para iterar sobre los parametros recibidos.
   if (argc < 2) {
      printf("Faltan parametros.\n Uso ./Ejercicio1 <param1> ... \n Ejemplo: ./Ej1 Ej1\n");
      return 0;
   }
   
   //Recorremos sobre todos los aparametros ingresados.
   while(n_archivos < argc){

      FILE *fp;//apuntador para abrir el archivo.
      int c, i = 0;//c es el valor del byte que se está leyendo, i un índice para guardar los valores
      int cabecera[10];//valores para comprobar tipo de archivo.
   
      fp = fopen(argv[n_archivos],"rb");
      //verificamos que el archivo existe.
      if(fp == NULL) {
         perror("Erro al abrir el archivo.");
         return(-1);
      } do {
         c = fgetc(fp);
         cabecera[i] = c; //guardamos el valor del byte ne la cabecera
         i++;

         if( feof(fp) ) {//terminamos en caso de llegar al final de archivo.
            break ;
         }
      } while(i < 10);
      fclose(fp);//cerramos el flujo de información 

      //Verificamos los valores guardados en la cabecera para el acchivo actual.
      //configuracion en hexa 47 49 46 38 37 61 ó 47 49 46 38 39 61
      if ((cabecera[0]==71 && cabecera[1]== 73 && cabecera[2]==70 &&cabecera[3]==56 && cabecera[4]==55 && cabecera[5] == 97)
         ||(cabecera[0]==71 && cabecera[1]== 73 && cabecera[2]==70 &&cabecera[3]==56 && cabecera[4]==57 && cabecera[5] == 97)) 
         printf("%s: archivo GIF.\n", argv[n_archivos]);

      //configuración en hexa 7F 45 4C 46
      else if (cabecera[0]==127 && cabecera[1]== 69 && cabecera[2]==76 && cabecera[3]==70)
         printf("%s: archivo ELF\n", argv[n_archivos]);

      //configuración en hexa 89 50 4E 47 0D 0A 1A 0A
      else if (cabecera[0]==137 && cabecera[1]== 80 && cabecera[2]==78 && cabecera[3]==71 
            && cabecera[4]==13&& cabecera[5]==10 && cabecera[6] == 26 &&cabecera[7] == 10) 
         printf("%s: archivo PNG\n", argv[n_archivos]);

      //configuración en hexa 50 4B 03 04 ó 50 4B 05 06 ó 50 4B 07 08
      else if ((cabecera[0]==80 && cabecera[1]== 75 && cabecera[2]==3 && cabecera[3]==4)
            || (cabecera[0]==80 && cabecera[1]== 75 && cabecera[2]==5 && cabecera[3]==6)
            || (cabecera[0]==80 && cabecera[1]== 75 && cabecera[2]==7 && cabecera[3]==8))
         printf("%s: archivo ZIP\n", argv[n_archivos]);

      //configuración en hexa 25 50 44 46 2d
      else if (cabecera[0]==37 && cabecera[1]== 80 && cabecera[2]==68 && cabecera[3]==70 && cabecera[4]==45)
         printf("%s: archivo PDF\n", argv[n_archivos]);

      //configuración en hexa FF FB ó 49 44 33
      else if ((cabecera[0]==255 && cabecera[1]== 251)
            ||(cabecera[0]==73 && cabecera[1]==68 && cabecera[2]==51))
         printf("%s: archivo MP3\n", argv[n_archivos]);

      //configuración en hexa 4D 5A
      else if (cabecera[0]==77 && cabecera[1]== 90)
         printf("%s: archivo EXE\n", argv[n_archivos]);

      else
         printf("%s archivo no identificado\n", argv[n_archivos]);
      
      n_archivos++;
   }
   
   
   
   
   

   return(0);
}