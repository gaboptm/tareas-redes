#include <openssl/md5.h>
#include <openssl/sha.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char *argv[]){
    unsigned char dmd5[MD5_DIGEST_LENGTH];
    unsigned char dsha1[SHA_DIGEST_LENGTH];
    unsigned char dsha256[SHA256_DIGEST_LENGTH];

    if(strcmp(argv[1],"md5")==0){
    	for (int j = 2; j < argc; ++j){
    		FILE* inFile;
    		if (inFile = fopen (argv[j], "rb")){
			    int i;
			    MD5_CTX mdContext;
			    int bytes;
			    unsigned char data[1024];
			    MD5_Init (&mdContext);
			    while ((bytes = fread (data, 1, 1024, inFile)) != 0)
			        MD5_Update (&mdContext, data, bytes);
			    MD5_Final (dmd5,&mdContext);
			    for(i = 0; i < MD5_DIGEST_LENGTH; i++) printf("%02x", dmd5[i]);
			    printf (" %s\n", argv[j]);
			    fclose (inFile);
			}
			else
				fprintf(stderr, "Error abriendo archivo %s\n",argv[j]);
		}
    }
    else if(strcmp(argv[1],"sha1") == 0){
    	for (int j = 2; j < argc; ++j){
    		FILE* inFile;
    		if (inFile = fopen (argv[j], "rb")){
			    int i;
			    SHA_CTX sha1Context;
			    int bytes;
			    unsigned char data[1024];
			    SHA1_Init (&sha1Context);
			    while ((bytes = fread (data, 1, 1024, inFile)) != 0)
			        SHA1_Update (&sha1Context, data, bytes);
			    SHA1_Final (dsha1,&sha1Context);
			    for(i = 0; i < SHA_DIGEST_LENGTH; i++) printf("%02x", dsha1[i]);
			    printf (" %s\n", argv[j]);
			    fclose (inFile);
			}
			else
				fprintf(stderr, "Error abriendo archivo %s\n",argv[j]);
		}
    }
    else if(strcmp(argv[1], "sha256") == 0){
    	for (int j = 2; j < argc; ++j){
    		FILE* inFile;
    		if (inFile = fopen (argv[j], "rb")){
			    int i;
			    SHA256_CTX sha256Context;
			    int bytes;
			    unsigned char data[1024];
			    SHA256_Init (&sha256Context);
			    while ((bytes = fread (data, 1, 1024, inFile)) != 0)
			        SHA256_Update (&sha256Context, data, bytes);
			    SHA256_Final (dsha256,&sha256Context);
			    for(i = 0; i < SHA256_DIGEST_LENGTH; i++) printf("%02x", dsha256[i]);
			    printf (" %s\n", argv[j]);
			    fclose (inFile);
			}
			else
				fprintf(stderr, "Error abriendo archivo %s\n",argv[j]);
		}
    }
    else{
    	fprintf(stderr,"Hash criptografico inválido\n");
    }
    // unsigned char 
    // char *filename="file.c";
    // int i;
    // FILE *inFile = fopen (filename, "rb");
    // MD5_CTX mdContext;
    // int bytes;
    // unsigned char data[1024];

}