# Práctica 5 (DOS INTEGRANTES)
*"en la clase se dijo que se entregaría individualmente, pero puede entregarse en equipos de 2 personas (en tal caso indicar los nombres de los integrantes en la documentación de la práctica"* - Andrea Itzel
## Alvarez Cerrillo Mijail Aron
### 310020590

## Gomez Gutierrez Hugo Alberto
### 310054236

### 2.DNS
#### Registramos subdominio
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p5/imagenes/p5-1.png)

#### Subdominio agregado con la zona delegada a los servidores DNS.
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p5/imagenes/1.png)

#### Agregamos nuevo registro de tipo A.
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p5/imagenes/2.png)

#### Comprobar con nslookup
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p5/imagenes/3.png)

#### Preguntas
##### ¿Qué es un servidor DNS?
A DNS server is a computer used to resolve hostnames to IP addresses.[^1]
##### ¿Para qué se utiliza cada uno de los registros SOA, NS, A, AAAA, PTR, CNAME, MX y TXT?
* SOA - The record specifies core information about a DNS zone, including the primary name server, the email of the domain administrator, the domain serial number, and several timers relating to refreshing the zone.
* NS - NS stands for 'name server' and this record indicates which DNS server is authoritative for that domain (which server contains the actual DNS records). [^4]
* A - specifies IP addresses corresponding to your domain and its subdomains;
* AAAA - The record AAAA (also quad-A record) specifies IPv6 address for given host.[^3]
* PTR - As opposed to forward DNS resolution (A and AAAA DNS records), the PTR record is used to look up domain names based on an IP address.
* MX - specifies where the emails for your domain should be delivered;
* CNAME - specifies redirects from your domain's subdomains to other domains / subdomains;
* TXT - these records are used to store text-based information related to your domain. One of their most common uses is for SPF data. SPF, or Sender Policy Framework, is an attempt to control forged e-mail.[^2]
##### ¿Qué hacen las herramientas nslookup y dig?
* nslookup is a network administration command-line tool available in many computer operating systems for querying the Domain Name System (DNS) to obtain domain name or IP address mapping, or other DNS records.[^5]
* Dig stands for (Domain Information Groper) is a network administration command-line tool for querying Domain Name System (DNS) name servers. It is useful for verifying and troubleshooting DNS problems and also to perform DNS lookups and displays the answers that are returned from the name server that were queried.[^6]
### 3.Packetracer
#### Conectar dos PCs a través de un switch, asginar direcciones y comprobar conexion.
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p5/imagenes/p5-2-1.png)

#### Conectar una PC, una impresora y un servidor a un swicht, (usando DHCP), comprobar conexion.
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p5/imagenes/p5-2-2.png)

#### Conectar dos servidores a un switch y verificar conexion
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p5/imagenes/p5-2-3.png)

### Hacemos los ajustes pertinenetes por los comandos de los routers para permitir la conexion y el paso de datos, para que pueda llegar la info a la PC1 y la PC3.
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p5/imagenes/p5-2-7.png)
![](https://gitlab.com/mxnmijail/tareas-redes/raw/master/content/tarea/MijailAlvarez/practicas/p5/imagenes/p5-2-8.png)

#### Preguntas:
##### ¿Qué diferencia hay entre un switch y un router?
The most basic explanation is that a switch is designed to connect computers within a network, while a router is designed to connect multiple networks together. [^7]
##### ¿Qué tipos de switches hay?
{LAN Switch, Unmanaged Switch, Managed Switch, PoE Switch, Stackable Switch}[^8]
##### ¿Qué es una lista de acceso y para qué se utilizan?
Access Control List (ACL) are filters that enable you to control which routing updates or packets are permitted or denied in or out of a network. They are specifically used by network administrators to filter traffic and to provide extra security for the network. This can be applied to routers (Cisco).[^9]
##### Se creó una lista de acceso con el comando access-list 1 permit 192.168.1.0 0.0.0.255, la última parte del comando (0.0.0.255) es una wildcard, ¿qué es una wildcard  y en qué se diferencia con una máscara de red?
Subnet mask-A 32-bit combination used to describe which portion of an address refers to the subnet/network and which part refers to the host.It is used along with IP Address.Wildcard Mask- To indicate to the IOS software whether to check or ignore corresponding IP address bits when comparing the address bits in an access list entry , OSPF/EIGRP network command..A wildcard mask is sometimes referred to as an inverted mask because a 1 and 0 mean the opposite of what they mean in a subnet (network) mask.[^13]
##### ¿Qué diferencia hay entre una dirección IP pública y una privada?
A public IP address is an IP address that can be accessed over the Internet. ... Private IP address, on the other hand, is used to assign computers within your private space without letting them directly expose to the Internet.[^10]
##### ¿Qué diferencia hay entre NAT estática, NAT dinámica y PAT? ¿Cuál es la que se encuentra usualmente en los routers de los hogares?
* Static NAT creates a fixed translation of private addresses to public addresses. ... The main difference between dynamic NAT and static NAT is that static NAT allows a remote host to initiate a connection to a translated host if an access list exists that allows it, while dynamic NAT does not.[^11]
* Port Address Translation (PAT) is another type of dynamic NAT which can map multiple private IP addresses to a single public IP address by using a technology known as Port Address Translation.[^12]

[^1]:https://www.lifewire.com/what-is-a-dns-server-2625854

[^2]:https://www.siteground.com/kb/dns_records_ns_a_mx_cname_spf_explained/

[^3]:http://dns-record-viewer.online-domain-tools.com/

[^4]:https://www.cloudflare.com/learning/dns/dns-records/dns-ns-record/

[^5]:https://en.wikipedia.org/wiki/Nslookup

[^6]:https://www.tecmint.com/10-linux-dig-domain-information-groper-commands-to-query-dns/

[^7]:https://pc.net/helpcenter/answers/difference_between_switch_and_router

[^8]:http://www.fiber-optical-networking.com/different-types-of-switches-in-networking.html

[^9]:https://www.orbit-computer-solutions.com/access-control-lists/

[^10]:https://www.iplocation.net/public-vs-private-ip-address

[^11]:https://www.cisco.com/c/en/us/td/docs/switches/datacenter/nexus3548/sw/interfaces/602_a1_1/b_N3548_Interfaces_Config_602_A1_1/b_N3548_Interfaces_Config_602_A1_1_chapter_0101.pdf

[^12]:http://www.omnisecu.com/cisco-certified-network-associate-ccna/static-nat-dynamic-nat-and-pat.php

[^13]:https://www.quora.com/What-is-the-difference-between-subnetmask-and-wildcard-in-networking
