/* Ppráctica 3 : Manejo de archivos binarios y captura de paquetes
   Alvarez Cerrillo Mijail Aron 310020590
   Este programa representa el tercer ejercicio de la practica 3:
   El programa deberá de recibir como argumento una cadena que indique qué tipo 
   de hash se utilizará, seguido de una lista de archivos cuyos hashes serán
   extraídos.*/
#include <openssl/md5.h>
#include <stdio.h>
#include <stdlib.h>

/* Funcion que imprime el error y termina con el programa */
void error(char *msg) {
  perror(msg);
  exit(1);
}

/* Ocupamos el md5 de openssl para poder hace hash a nuestros archivos,
   abrimos un archivo y lo recorremos byte por byte (fread), de este modo
   al cambiar un solo byte de dicho archivo, cambia el resultado que arroja md5
   sobre el archivo en cuestion. Fuentes que se utilizaron para la realizacion 
   de este ejercicio: https://www.openssl.org/docs/man1.1.0/man3/MD5.html y
   https://www.lynxbee.com/c-program-to-calculate-md5-of-a-file-and-check-if-it
   -matches-with-predefined-downloaded-md5sum/ */
void md5(char *fileN) {
    unsigned char c[MD5_DIGEST_LENGTH];
    int i;
    //Lectura de archivos binarios ["rb"]
    FILE *inFile = fopen (fileN, "rb");
    MD5_CTX mdContext;
    int bytes;
    unsigned char data[1024];

    if (inFile == NULL)
      error("Error de lectura de archivo");
    
    MD5_Init (&mdContext);
    while ((bytes = fread (data, 1, 1024, inFile)) != 0)
        MD5_Update (&mdContext, data, bytes);
    MD5_Final (c,&mdContext);
    for(i = 0; i < MD5_DIGEST_LENGTH; i++) printf("%02x", c[i]);
    printf ("  %s\n", fileN);
    fclose (inFile);
}

/* Funcion main que sirve para ejecutar el hash md5 a multimples archivos
   ingresados como argumentos del programa*/
int main(int argc, char *argv[]) {
  if(argc < 2)
    error("Porfavor ingresa md5 seguido del los archivos a aplicar el hash.");
  else
    for(int i = 2; i < argc; i++)//Implementar los restantes.(TODO)
      md5(argv[i]);
  return 0;
}
