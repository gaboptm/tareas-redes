# Universidad Nacional Autónoma de México
## Facultad de ciencias
### Redes de computadoras
### Practica 1

#### Compilación
Para compilar y ejecutar se debe de abrir la terminal y colocarse en el directorio donde se encuentra el código fuente. Una vez ahí escribir el comando "make" y una vez escrito se ejecuta el programa.
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p1/imagenes/compilar.png)

#### Agregar archivos
Una vez en el menú del programa para agregar archivos poner el número 1 en la pantalla y escribir el nombre del archivo.
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p1/imagenes/opcion1.png)

#### Leer archivos
Para leer un archivo es la opción dos del menú. Si no hay ningún archivo agregado manda error por no tener archivos, si hay un archivo se solicitara el número del archivo, el primer archivo es el número 0.
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p1/imagenes/opcion2_1.png)


#### Eliminar archivos
Para eliminar un archivo es la opción tres del menú. Si no hay ningún archivo agregado manda error por no tener archivos, si hay un archivo o más se solicitara el número del archivo, el primer archivo es el número 0.
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p1/imagenes/eliminar.png)

#### Imprimir  archivos
Para imprimir en pantalla los archivos es con la opción cuatro del menú. Si no hay ningún archivo agregado muestra de inmediato el menú, en caso de que si haya archivos los muestra.
