# Universidad Nacional Autónoma de México
## Facultad de ciencias
### Redes de computadoras
### Practica 3

#### Compilación
Para compilar y ejecutar se debe de abrir la terminal y colocarse en el directorio donde se encuentra el código fuente. Una vez ahí escribir el comando "make todos".
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p3/imagenes/Captura%20de%20pantalla%20de%202019-03-24%2015-31-06.png)

#### File
Para poner ejecutar este programa, nos debemos ir al directorio donde compilamos nuestro programa y ejecutar el comando "./magia ..." agregando los archivos que deseemos saber su extensión. 
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p3/imagenes/Captura%20de%20pantalla%20de%202019-03-24%2015-32-28.png)
#### Strings
Para poner ejecutar este programa, nos debemos ir al directorio donde compilamos nuestro programa y ejecutar el comando "./strings ..." agregando el archivo que deseamos saber las cadenas conformadas por caracteres imprimibles consecutivos dentro de un archivo binario.
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p3/imagenes/Captura%20de%20pantalla%20de%202019-03-24%2015-33-27.png)
#### Hashes
Para poner ejecutar este programa, nos debemos ir al directorio donde compilamos nuestro programa y ejecutar el comando "./hashes md5 ...", "./hashes sha1 ..." y "./hashes sha256 ...". Podemos ejecutar nuestro programa con cualquier opción, después de el tipo de hash van los archivos a los que deseamos saber su hash.
![MD5](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p3/imagenes/Captura%20de%20pantalla%20de%202019-03-24%2016-53-23.png)
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p3/imagenes/Captura%20de%20pantalla%20de%202019-03-24%2016-53-32.png)
![](https://gitlab.com/hugo_gomez/tareas-redes/raw/master/content/tarea/HugoGomez/practicas/p3/imagenes/Captura%20de%20pantalla%20de%202019-03-24%2016-53-39.png)

