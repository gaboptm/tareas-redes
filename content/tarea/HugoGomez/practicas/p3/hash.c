#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/md5.h>
#include <openssl/sha.h>

void sacarSha1(char nomArc[50]);
void sacarMd5(char nomArch[50]);
void sacarSha256(char nomArc[50]);

int main(int argc, char **argv){

    if(argc >= 3){
      int i = 2;
      while(i<argc){
        if(strcmp(argv[1], "md5")==0){
            sacarMd5(argv[i]);
        }else if(strcmp(argv[1], "sha1")==0){
          sacarSha1(argv[i]);
        }else if(strcmp(argv[1], "sha256")==0){
          sacarSha256(argv[i]);
        }else{
          printf("opción invalida\n");
          exit(0);
        }
        i = i + 1;
      }

    }
    return 0;
}

void sacarSha256(char archivo[50]){
  int c;
  char *tempo = malloc(40000);
  FILE *fd;
  fd = fopen(archivo, "rb");
  if(fd == NULL){
    printf("sha256: %s: No existe el archivo o el directorio\n", archivo);
    return ;
  }
  SHA256_CTX ctx;
  SHA256_Init(&ctx);
  while((c = fread(tempo, 1, 40000, fd))){
    SHA256_Update(&ctx, tempo, c);
  }
  char hash[SHA256_DIGEST_LENGTH];
  SHA256_Final(hash, &ctx);

  char salidaS[65];
  int i = 0;
  for(i = 0; i < SHA256_DIGEST_LENGTH; i++){
    sprintf(salidaS + (i * 2), "%02x", (unsigned char)hash[i]);
  }
  printf("%s   %s\n", salidaS, archivo);

  fclose(fd);
}


void sacarSha1(char nomArch[50]){

  int c;
  char *tempo = malloc(40000);
  FILE *fd;
  fd = fopen(nomArch, "rb");
  if(fd == NULL){
    printf("sha1: %s: No existe el archivo o el directorio\n", nomArch);
    return ;
  }
  SHA_CTX ctx;
  SHA1_Init(&ctx);
  while((c = fread(tempo, 1, 40000, fd))){
    SHA1_Update(&ctx, tempo, c);
  }
  char hash[SHA_DIGEST_LENGTH];
  SHA1_Final(hash, &ctx);

  char salidaS[65];
  int i = 0;
  for(i = 0; i < SHA_DIGEST_LENGTH; i++){
    sprintf(salidaS + (i * 2), "%02x", (unsigned char)hash[i]);
  }
  printf("%s   %s\n", salidaS, nomArch);

  fclose(fd);

}

void sacarMd5(char nomArch[50]){

  int c;
  char *tempo = malloc(40000);
  FILE *fd;
  fd = fopen(nomArch, "rb");
  if(fd == NULL){
    printf("md5: %s: No existe el archivo o el directorio\n", nomArch);
    return ;
  }
  MD5_CTX ctx;
  MD5_Init(&ctx);
  while((c = fread(tempo, 1, 40000, fd))){
    MD5_Update(&ctx, tempo, c);
  }
  char hash[MD5_DIGEST_LENGTH];
  MD5_Final(hash, &ctx);

  char salidaS[65];
  int i = 0;
  for(i = 0; i < MD5_DIGEST_LENGTH; i++){
    sprintf(salidaS + (i * 2), "%02x", (unsigned char)hash[i]);
  }
  printf("%s   %s\n", salidaS, nomArch);

  fclose(fd);

}
