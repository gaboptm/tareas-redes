# Práctica 6
## Integrantes:
* Cova Pacheco Felipe de Jesús
* Calderón Fernández Gabriel

1 )

```apt install net-tools sudo wget curl bash-completion```

Una vez que se haya aplicado el nombre de host de la máquina, inicie sesión nuevamente con privilegios de raíz e instale el servidor HTTP Apache, que, de manera predeterminada, se precompila en un paquete binario y lo proporcionan los repositorios de Debian 9.

```apt install apache2```

A continuación, instale la última versión del intérprete de lenguaje PHP y todos los módulos necesarios necesarios para el servidor web Apache para implementar Wordpress CMS emitiendo el siguiente comando.

```apt install libapache2-mod-php7.0 php7.0 php7.0-gd php7.0-xml php7.0-curl php7.0-mbstring php7.0-mcrypt php7.0-xmlrpc```

El último componente que falta ahora para que se complete la pila LAMP es el servidor de base de datos MariaDB. Ejecute el siguiente comando para instalar el servidor MariaDB junto con el módulo PHP requerido para acceder a la base de datos desde el servidor HTTP Apache.

```apt install php7.0-mysql mariadb-server mariadb-client```

![Imagen 1](1.png)

![Imagen 2](2.png)

```systemctl start mariadb```

**```mysql_secure_installation```**

En el siguiente paso, habilitaremos y configuraremos TLS y reescribiremos los módulos para el servidor web Apache, que de forma predeterminada están desactivados. Ejecute los siguientes comandos para habilitar ambos módulos.

```a2enmod rewrite ssl```

```a2ensite default-ssl.conf```

Luego, abra los archivos de configuración de Apache para ambos sitios habilitados y agregue el siguiente bloque debajo de la directiva **DocumentRoot** como se ilustra en la siguiente captura de pantalla.

```nano /etc/apache2/sites-enabled/000-default.conf```

```nano /etc/apache2/sites-enabled/default-ssl.conf```

En ambos archivos de configuración agregue el siguiente bloque de código:
```
<Directory / var / www / html>
  Índices de opciones FollowSymLinks MultiViews
  AllowOverride All
Require all otorgado
</Directory>
```

![Imagen 3](3.png)

![Imagen 4](4.png)

![Imagen 5](5.png)

PUNTO EXTRA

En el archivo de configuración TLS de apache **default-ssl.conf** también, para mejorar la seguridad del protocolo SSL de Apache. Estas líneas de código solucionan las vulnerabilidades comunes de SSL.

![Imagen 6](6.png)

```a2enmod headers```

```systemctl restart apache2.service```

Después de haber realizado todos los cambios anteriores, pruebe la configuración de Apache para detectar posibles errores de sintaxis y si todo está bien, reinicie los demonios LAMP para reflejar todos los cambios realizados hasta ahora y habilite los servicios en todo el sistema emitiendo los siguientes comandos

```apache2ctl -t```

```systemctl restart apache2.service mariadb.service```

```systemctl enable apache2.service mariadb.service```

Finalmente, verifique si los servicios Apache y MariaDB han abierto puertos en estado de escucha emitiendo el comando netstat y filtre la salida con egrep. Ahora, el servidor web Apache debería tener los puertos 80 y 443 (correspondientes a los protocolos HTTP y https)

![Imagen 7](7.png)

Para asegurarse de que se pueda acceder a su servidor web desde Internet a través de los protocolos HTTP y HTTPS

![Imagen 8](8.png)

Para probar si PHP está funcionando como se espera en el lado del servidor, cree un PHP **php** en **/ var / www / html /** system, que es la ruta predeterminada de la raíz del documento web del servidor web Apache, emitiendo el siguiente comando y navegue hasta el siguiente URI del navegador para obtener el resultado: http://localhost/info.php

**_```echo '<?php phpinfo(); ?>' > /var/www/html/info.php```_**

![Imagen 9](9.png)

## Instalar WordPress 5.0

```
mysql -u root -p
CREATE DATABASE wordpress;
GRANT ALL PRIVILEGES ON wordpress.* TO
'wordpress-user'@'localhost' IDENTIFIED BY 'wordpress-pass';
FLUSH PRIVILEGES;
```

![Imagen 10](10.png)

![Imagen 11](11.png)

A continuación, descargue el último archivo comprimido de Wordpress del sitio web oficial con la utilidad wget y extraiga el archivo comprimido con la utilidad tar . Copie los archivos de instalación en la ruta de la raíz del documento del servidor web Apache emitiendo los siguientes comandos. Además, asegúrate de eliminar la página web index.html predeterminada.

```
cd /tmp
wget http://wordpress.org/latest.tar.gz
tar xfz latest.tar.gz
cp -rf wordpress/* /var/www/html/
rm /var/www/html/index.html
```

![Imagen 12](12.png)

![Imagen 13](13.png)

![Imagen 14](14.png)

![Imagen 15](15.png)

Modifique los permisos de la ruta de Apache webroot como se describe a continuación para otorgar permisos de escritura completos al grupo de datos web www-data en el directorio de instalación. Esta configuración permitirá al servidor web Apache crear o modificar archivos y directorios bajo esta ruta específica del sistema. Los más importantes serían el directorio de cargas y el archivo .htaccess , que se pueden usar para controlar y extender la funcionalidad de Apache.

```
chmod -R 775 /var/www/html/
chgrp -R www-data /var/www/html/
ls -al /var/www/html
```

![Imagen 16](16.png)

![Imagen 17](17.png)

En la siguiente pantalla de instalación, agregue el nombre de la base de datos, las credenciales de acceso a la base de datos y el host de la base de datos que creó anteriormente para la instalación de Wordpress y presione el botón Enviar para continuar. La configuración de la base de datos se guardará en /var/www/html/wp-config.php . Puede modificar manualmente el archivo en una fecha posterior para propósitos especiales de Wordpress.

![Imagen 18](18.png)

En la siguiente pantalla, haga clic en Ejecutar el botón de instalación y complete el título de su sitio web, las credenciales de administrador del sitio web y la dirección de correo electrónico. Los scripts de instalación deben generar automáticamente una contraseña segura. Puede elegir guardar esta contraseña o proporcionar su propia contraseña segura. Cuando hayas terminado, presiona el botón Instalar Wordpress para completar el proceso de instalación.

![Imagen 19](19.png)

![Imagen 20](20.png)

![Imagen 21](21.png)

![Imagen 22](22.png)

Finalmente, después de que la instalación se complete con éxito, inicie sesión en el panel de Wordpress con las credenciales creadas durante el proceso de instalación y comience a administrar su propio sitio web.

![Imagen 23](23.png)

![Imagen 24](24.png)

![Imagen 25](25.png)

### AGREGANDO SIMPLE CACHE
Instalamos un servidor ftp en nuestra maquina ya que wordpress necesita de
ello.

![Imagen 26](26.png)

![Imagen 27](27.png)

Agregamos un zip que descargamos de: https://wordpress.org/plugins/simple-cache/

![Imagen 28](28.png)

![Imagen 29](29.png)

![Imagen 30](30.png)

![Imagen 31](31.png)

![Imagen 32](32.png)

Activamos REDIS

![Imagen 33](33.png)

![Imagen 34](34.png)

Agregamos el script en la carpeta

![Imagen 35](35.png)

Y de esta manera tenemos un servicio de cache usando Simple Cache integrandole REDIS.

https://linuxconfig.org/how-to-configure-ftp-server-on-debian-9-stretch-linux

https://tecadmin.net/install-mysql-server-on-debian9-stretch/

https://wphowto.net/how-to-enable-caching-in-wordpress-1807

https://www.howtoforge.com/tutorial/install-wordpress-5-with-apache-on-debian-9/
