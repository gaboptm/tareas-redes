# Práctica 4
## Cova Pacheco Felipe de Jesús

### Creación de red virtual
![Imagen 1](1.jpg)
![Imagen 2.1](2.1.jpg)
![Imagen 2.2](2.2.jpg)
### Configuración de adaptador de red
![Imagen 3](3.jpg)
### Configuración de interfaz de red
![Imagen 4](4.jpg)
Asignar dirección IP de manera estática a interfaz en archivo
![Imagen 5](5.jpg)
Reiniciar servicio de red
![Imagen 6](6.jpg)
Verificar que la interfaz de red tiene la dirección IP asignada
![Imagen 7](7.jpg)
Agregar la siguiente línea a archivo /etc/resolv.conf:
![Imagen 8](8.jpg)
Verificar que se tiene acceso a internet con ping
![Imagen 9](9.jpg)
### Servidor DHCP
Instalar servidor DHCP
![Imagen 10](10.jpg)
![Imagen 11](11.jpg)
![Imagen 12](12.jpg)
![Imagen 13.1](13.1.jpg)
![Imagen 13.2](13.2.jpg)
![Imagen 13.3](13.3.jpg)
Crear máquina virtual cliente con Debian 9, repetir la **Configuración de adaptador de red** para esta máquina.
Capturar tráfico de red con Wireshark o tcpdump (guardando tráfico en un archivo). El tráfico de red debe de ser únicamente el de la red virtual que fue creada, notar que en la máquina física se creó una interfaz de red virtual asociada a ésta.
Prender máquina y verificar que se le asignó una dirección IP en el rango de direcciones IP asignadas por el servidor DHCP.
![Imagen 14](14.jpg)
![Imagen 15](15.jpg)
### PXE
![Imagen 16](16.jpg)
![Imagen 17](17.jpg)
Crear una nueva máquina virtual
![Imagen 18](18.jpg)
![Imagen 19](19.jpg)
![Imagen 20](20.jpg)
![Imagen 21](21.jpg)
![Imagen 22](22.jpg)
![Imagen 23](23.jpg)
![Imagen 24](24.jpg)
![Imagen 25](25.jpg)
Seleccionar la red virtual creada para el adaptador de red
![Imagen 26](26.jpg)
Capturar en un archivo el tráfico de la red virtual, prender máquina y observar que se arranca con la imagen del sistema operativo
![Imagen 27](27.jpg)
Terminar captura de tráfico, observar cómo se lleva a cabo el intercambio del protocolo DHCP junto con la descarga de archivos para el arranque por medio del protocolo TFTP
![Imagen 28](28.jpg)

## Preguntas
### ¿Cómo funciona el protocolo DHCP?
DHCP funciona sobre un servidor central (servidor, estación de trabajo o
incluso un PC) el cual asigna direcciones IP a otras máquinas de la red. Este
protocolo puede entregar información IP en una LAN o entre varias VLAN.
Esta tecnología reduce el trabajo de un administrador, que de otra manera
tendría que visitar todos los ordenadores o estaciones de trabajo uno por
uno. Para introducir la configuración IP consistente en IP, mascara, gateway,
DNS, etc. Un servidor DHSC (DHCP Server) es un equipo en una red que está
corriendo un servicio DHCP. Dicho servicio se mantiene a la escucha de
peticiones broadcast DHCP. Cuando una de estas peticiones es oída, el
servidor responde con una dirección IP y opcionalmente con información
adicional.
### ¿Cómo se filtra el tráfico del protocolo DHCP en una captura de tráfico en Wireshark y en tcpdump?
En wireshark:
udp.port==68
y en tcpdump:
se pone en la terminal:
sudo tcpdump -i “interface” port 67 or port 68 -e -n
### ¿Qué es el estándar PXE y cuál es su utilidad?
El estándar Preboot eXecution Environment (PXE) (Entorno de ejecución de
prearranque), es un entorno para arrancar e instalar el sistema operativo en
computadoras a través de una red, de manera independiente de los
dispositivos de almacenamiento de datos disponibles (como discos duros) o
de los sistemas operativos instalados.
Funcionamiento:
El firmware del cliente trata de encontrar un servicio de redirección PXE en la
red para recabar información sobre los servidores de arranque PXE
disponibles. Tras analizar la respuesta, el firmware solicitará al servidor de
arranque apropiado el file path de un network bootstrap program (NBP), lo
descargará en la memoria RAM del computador mediante TFTP,
probablemente lo verificará, y finalmente lo ejecutará. Si se utiliza un único
NBP para todos los clientes PXE se puede especificar mediante BOOTP sin
necesidad de un proxy DHCP, pero aún será necesario un servidor TFTP.
