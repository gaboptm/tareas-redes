#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <sys/stat.h>
#include <sys/sendfile.h>
#include <fcntl.h>
#include <arpa/inet.h>
#define MAX 1024
#define SA struct sockaddr

void comunicacion(int sockfd)
{
  char buff[MAX];

  while(1) {

    bzero(buff, MAX);
    read(sockfd, buff, sizeof(buff));
    if (strncmp("EOF", buff, 3) == 0) { break; }

    FILE * fptr;
    char c;

    strcat(buff, " &> src/temp.txt");

    system(buff);
    fptr = fopen("src/temp.txt", "r");

    char tmp[MAX];
    char buff2[MAX];
    bzero(buff2, MAX);
    while(fgets(tmp, sizeof(tmp), fptr)) {
      strcat(buff2,tmp);
    }
    fclose(fptr);
    remove("temp.txt");

    write(sockfd, buff2, sizeof(buff2));
  }
}

int main(int argc, char **argv)
{
  int sockfd, connfd, len;
  struct sockaddr_in servaddr, cli;

  sockfd = socket(AF_INET, SOCK_STREAM, 0);
  if (sockfd == -1) {
    printf("Error en creación de socket\n");
    exit(1);
  }
  bzero(&servaddr, sizeof(servaddr));

  if(argc != 2) {
    printf("# incorrecto de argumentos\n");
    exit(1);
  }

  int port = atoi(argv[1]);

  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  servaddr.sin_port = htons(port);

  if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) {
    printf("Enlace fallida\n");
    exit(1);
  }

  if ((listen(sockfd, 5)) != 0) {
    printf("Error al escuchar\n");
    exit(1);
  }

  len = sizeof(cli);

  connfd = accept(sockfd, (SA*)&cli, &len);
  if (connfd < 0) {
    printf("Error al aceptar la conexión\n");
    exit(1);
  }

  comunicacion(connfd);
  close(sockfd);
}
