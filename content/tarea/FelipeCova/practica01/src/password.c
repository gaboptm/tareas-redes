#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

void imprime_info(char* []);

int main(int argc, char **argv)
{
  FILE *fp;

  if(argc != 2)
  {
    printf("Modo de uso: ./password <nombre_del_archivo>\n");
    exit(1);
  }

  char *archivo = argv[1];

  if ((fp = fopen(archivo,"r")) == NULL) {
    fprintf(stderr, "Problemas al abrir el archivo. Verificar que existe.\n");
    exit(1);
  }

  char line[1024];
  while (fgets(line, sizeof(line), fp))
  {
    char* arr[7];
    char *token;
    char *lineAux;

    lineAux = strdup(line);

    int i = 0;
    while((token = strsep(&lineAux,":")) != NULL)
    {
      arr[i] = token;
      i++;
    }

    imprime_info(arr);
  }
  fclose(fp);

  return 0;
}

void imprime_info(char* arr[])
{
  char* pass;
  if(strcmp(arr[1],"x") == 0) { pass = "Contraseña en archivo /etc/shadow"; }
  else if(strcmp(arr[1],"") == 0) { pass = "Autenticación sin contraseña"; }
  else { pass = arr[1]; }

  arr[1] = pass;
  printf("Nombre de usuario: %s\nContraseña: %s\nUID: %s\nGID: %s\nGECOS: %s\nHome: %s\nShell: %s\n",
    arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6]);
}
