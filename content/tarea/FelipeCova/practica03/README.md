## Cova Pacheco Felipe de Jesús
##### Compilar:
make
##### Eliminar ejecutables:
make clean
##### Ejecutar:
1. Magia "file": ./magia < archivo > < lista de archivos >
2. Caracteres imprimibles "strings": ./strings < archivo >
3. Hashes: ./hashes < archivo > < lista de archivos >

##### Compilar
![Imagen 1](screenshots/im1.png)
### Comando file
![Imagen 2](screenshots/im2.png)
### Comando strings
![Imagen 3](screenshots/im3.png)
### Hashes
##### MD5
![Imagen 4](screenshots/im4.png)
##### SHA1
![Imagen 5](screenshots/im5.png)
##### SHA256
![Imagen 6](screenshots/im6.png)
