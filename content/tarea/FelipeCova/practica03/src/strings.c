#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int long_archivo(char*);

int main(int argc, char **argv)
{
  if(argc != 2) 
  {
    fprintf(stderr, "Incluir archivo!\n");
    exit(1);
  }
  
  int longitud = long_archivo(argv[1]);
  
  unsigned char buffer[longitud];
  FILE *fp;
  fp = fopen(argv[1], "rb");
  fread(buffer, sizeof(buffer), 1, fp);
    
  int i,j;
  for(i = 0, j = 1; i < longitud, j < longitud; i++, j++)
  {
    if(buffer[i] >= 32 && buffer[i] <= 126) { 
      printf("%c", (char)buffer[i]); 
      if(buffer[j] < 32 || buffer[j] > 126) { printf("\n"); }
    }   
    
  }
  
  fclose(fp);
  
  return 0;
}

int long_archivo(char *archivo)
{
  FILE *fp;
  int longitud;
  fp = fopen(archivo, "r");
  
  if(fp == NULL) {
    fprintf(stderr,"Error al abrir archivo: No existe el archivo o directorio\n");
    exit(1);
  }
  
  fseek(fp, 0, SEEK_END);
  longitud = ftell(fp);
  fclose(fp);
  
  return longitud;
}