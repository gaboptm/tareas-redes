 #include <stdio.h>
 #include <stdlib.h> 
// #include <ncurses.h>
 #include <string.h>

 struct _archivador {
        char nombre[20];
        struct _archivador *siguiente;
        };
 
 struct _archivador *primero, *ultimo;


 void mostrar_lista() {
      struct _archivador *auxiliar; 
      int i;
 
      i=0;
      auxiliar = primero;
      printf("\nMostrando la lista completa:\n");
      while (auxiliar!=NULL) {
            printf( "Archivo guardado como: %s\n",
                    auxiliar->nombre);
            auxiliar = auxiliar->siguiente;
            i++;
      }
      if (i==0) printf( "\nLa lista est� vac�a!!\n" );
 }

 
 void anadir_archivo() {
      struct _archivador *nuevo;
 
      nuevo = (struct _archivador *) malloc (sizeof(struct _archivador));
      if (nuevo==NULL) printf( "No hay memoria disponible!\n");
 
      printf("\nGuardar Archivo:\n");
      printf("�C�mo quieres guardarlo?: "); fflush(stdout);
      //gets(nuevo->nombre);
      scanf("%s", nuevo->nombre);
      nuevo->siguiente = NULL;
 
      if (primero==NULL) {
         printf( "Primer archivo\n");
         primero = nuevo;
         ultimo = nuevo;
         }
      else {
           ultimo->siguiente = nuevo;
           ultimo = nuevo;
      }
 }
 

 
 void mostrar_menu() {
      printf("\n\nPractica01 Redes de computadoras:\n=====\n\n");
      printf("1.- Insertar archivo\n");
      printf("2.- Leer archivo\n");
      printf("3.- Imprimir archivos\n");
      printf("4.- Salir\n\n");
      printf("Escoge una opci�n: ");fflush(stdout);
 }


 int main() {
     int opcion;
     //initscr();
     //timeout(-1);

     primero = (struct _archivador *) NULL;
     ultimo = (struct _archivador *) NULL;
     do {

       //initscr();
       //timeout(-1);
         mostrar_menu();
	 scanf("%d", &opcion);
         //opcion = getch();
             switch ( opcion ) {
                case 1:
		  
		  anadir_archivo();
		  // timeout(-1);
                       break;
                case 2:  printf("No disponible todav�a!\n");
                        break;
                case 3: mostrar_lista(primero);
                        break;
                case 4: exit( 1 );
                default: printf( "Opci�n no v�lida\n" );
                         break;
             }
     } while (opcion!='4');
     //endwin();
 }
