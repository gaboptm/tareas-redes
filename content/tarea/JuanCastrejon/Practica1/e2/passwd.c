#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char **argv)
{
  if (argc != 2)
  {
    printf("Uso: %s <archivo_passwd>\n", argv[0]);
    return 1;
  }

  FILE *fp;

  fp = fopen(argv[1], "r");
  if (fp != NULL)
  {
    char line[200];
    char *token;
    while (fgets(line, sizeof line, fp) != NULL)
    {
      if (line[0] == '#')
        continue;
      int i = 0;
      char *s = line;
      while ((token = strsep(&s, ":")) != NULL)
      {
        switch(i) {
          case 0:
            printf("Nombre del usuario: %s\n", token);
            break;
          case 1:
            if(strcmp(token, "x") == 0)
              printf("Contraseña: Contraseña en archivo /etc/shadow\n");
            else if(strcmp(token, "") == 0)
              printf("Contraseña: Autenticación sin contraseña\n");
            else
              printf("Contraseña: %s\n", token);
            break;
          case 2:
            printf("UID: %s\n", token);
            break;
          case 3:
            printf("GID: %s\n", token);
            break;
          case 4:
            printf("GECOS: %s\n", token);
            break;
          case 5:
            printf("Home: %s\n", token);
            break;
          case 6:
            printf("Shell: %s\n", token);
            break;
        }
        i++;
      }
      free(s);
    }
    fclose(fp);
  }
  else
  {
    perror(argv[1]);
  }
}
