#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "lista.h"

int main()
{
  int option, i, n;
  char s[100], c;
  FILE *fp;
  struct lista lista = {.cabeza = 0, .longitud = 0};

  while (1)
  {
    printf("\n1. Insertar archivo\n");
    printf("2. Leer archivo\n");
    printf("3. Eliminar archivo\n");
    printf("4. Imprimir archivos\n");
    scanf("%d", &option);

    switch (option)
    {
    case 1:
      printf("Nombre del archivo: ");
      scanf("%s", s);
      char *e = (char *)malloc(sizeof(e) * strlen(s));
      strcpy(e, s);
      agrega_elemento(&lista, e);
      break;

    case 2:
      printf("Numero del archivo: ");
      scanf("%d", &n);
      if (!obten_elemento(&lista, n)) {
        printf("Numero no válido.\n");
        break;
      }
      fp = fopen(obten_elemento(&lista, n), "r");
      printf("\n");
      if (fp == NULL)
      {
        perror("No se pudo abrir el archivo.");
        break;
      }
      while ((c = fgetc(fp)) != EOF)
        printf("%c", c);
      printf("\n");
      break;

    case 3:
      printf("Numero del archivo: ");
      scanf("%d", &n);
      elimina_elemento(&lista, n);
      break;

    case 4:
      printf("\n");
      for (i = 0; i < lista.longitud; i++)
      {
        printf("%d: %s\n", i, obten_elemento(&lista, i));
      }
      break;

    default:
      printf("Opción no válida.\n");
    }
  }
  return 0;
}