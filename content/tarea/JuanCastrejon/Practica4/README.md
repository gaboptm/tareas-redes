# Práctica 4: DHCP

### Servidor DHCP

Se realizó la instalación de una maquina virtual con Debian 9 usando el software VMware. Una vez completada la instalación se realizó un clon identico, pero con una dirección MAC distinta para poder distinguir ambos equipos:

![](imgs/1.png)

Creación de la red virtual NAT dentro del software VMware, deshablitando el servicio DHCP:

![](imgs/3.png)

Asignación del adaptador de red virtual creada a ambas maquinas Debian:

![](imgs/2.png)

Configuraciñon de red estática sobre la primera máquina virtual:

![](imgs/4.png)

Reinicio del serivio de networking y verificación de la configuración de red:

![](imgs/5.png)

Comprobación de acceso a internet mediante el comando `ping google.com`:

![](imgs/6.png)

Una vez instalado el paquete `isc-dhcp-server` mediante el comando `# apt install isc-dhcp-server`
se realizó la configuración del servidor dhcp sobre el archivo `/etc/dhcp/dhcpd.conf`:

![](imgs/7.png)

Desactivación del uso de interfaz IPv6 en el arhivo `/etc/default/isc-dhcp-server`, se borró el contenido del archivo `/etc/dhcp/dhcpd6.conf` y se intentó reiniciar el servicio del servidor dhcp:

![](imgs/8.png)

Al intentar reiniciar un archivo de encontró un error. Después de investigar las causas del error, se logró reiniciar el servicio dhcp, usando el siguiente comando:

![](imgs/9.png)

Antes de encender la segunda máquina virtual para comprobar la asignación de IP, ocurrió un problema para detectar la interfaz vinculada a las máquinas virtuales. Se resolvió usando los siguientes comandos en el ambiente Windows, los cuales reinician el servicio npf para reindexar las interfaces y que sean detectables por Wireshark:

![](imgs/10.png)

Interfaces detectadas por Wireshark:

![](imgs/11.png)

Captura de tráfico correspondiente a la asignación de IP usando el servidor DHCP:

![](imgs/12.png)

Contenido de la bitácora de sistema en la máquina servidor:

![](imgs/13.png)

### PXE

Modificación del archivo `/etc/dhcp/dhcpd.conf` para soportar el booteo remoto y servir el archivo de imagen con el protocolo *tftp*:

![](imgs/14.png)

Reinicio del servicio `isc-dhcp-server`e instalación del paquete `tftpd-hpa`

![](imgs/15.png)

Luego de crear e iniciar una máquina virtual sin imagen de inicio se obtuvo el arranque de la imagen de inicio en la máquina virtual, usando el servidor dhcp y tftp.

![](imgs/17.png)

Captura de tráfico correspondiente al servicio de la imagen de arranque a la maquina destino:

![](imgs/16.png)

### Archivos de captura:

[DHCP](capturas/dhcp.pcapng)

[PXE](capturas/pxe.pcapng)

### Preguntas

- ¿Cómo funciona el protocolo DHCP?
> El servidor asigna dinámicamente una dirección IP junto con otros parámetros de red a los dispositivos de una red. Posee una lista de direcciones dinámicas y las va a asignando a los clientes conforme estas van quedando libres.
- ¿Cómo se filtra el tráfico del protocolo DHCP en una captura de tráfico en Wireshark y en tcpdump?
> Wireshark: udp.port == 68 que significa mostrar los paquetes que usen el puerto 68 o 67(conocido del DHCP, también puede ser el puerto 67).

>tcpdump: tcpdump -i <network-interface\> port 68 -e -n
- ¿Qué es el estándar PXE y cuál es su utilidad?
> Es un método para logar que una computadora cliente pueda iniciar usando sólo su tarjeta de red. Permite realizar un ciclo de arranque distinto para identificar problemas en la maquina, entre otros usos.