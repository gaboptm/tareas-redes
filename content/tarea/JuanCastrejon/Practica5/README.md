# Práctica 5: Inyección de paquetes, DNS y Packet Tracer

### 1. Inyección de tramas

Ejemplo de ejecución del inyector de tramas a la dirección MAC `AA:BB:CC:DD:EE:FF` desde `01:01:01:01:01:01` a través de la interfaz `en0`. Se usó el comando `$ sudo tcpdump ether host 01:01:01:01:01:01` para captar los paquetes provenientes de la direccion MAC especificada:

![](imgs/26.png)

### 2. DNS

Después del registro del domino `hrmp.cf` en la página www.freenom.com se configuraron los servidores DNS de `dns-he.net`:

![](imgs/24.png)

Se agregó el registro tipo A en `dns-he.net`:

![](imgs/25.png)

Comprobación de resolución de nombre de host a la IP asignada, usando `nslookup`:

![](imgs/27.png)

#### Preguntas

- ¿Qué es un servidor DNS?
> Es una computadora que funciona como base de datos de direcciones IP públicas y nombres de dominio asociados a estas. Sirve para resolver y traducir esos nombres a su IP.
- ¿Para qué se utiliza cada uno de los registros SOA, NS, A, AAAA, PTR, CNAME, MX y TXT?
>SOA: Especifica información autoritaria sobre una zona DNS.
>
>NS: Delega una zona DNS para utilizar los servidores de nombre autoritarios dados.
>
>A: Devuelve una dirección IPv4 de 32 bits. Es la más utilizada para asignar nombres de host a una dirección IP del host.
>
>AAAA: Devuelve una dirección IPv6 de 128 bits. Es la más utilizada para asignar nombres de host a una dirección IP del host.
>
>PTR: Apuntador a un nombre canónico. El uso más común es para implementar una consulta reversa DNS
>
>CNAME: Alias de un nombre a otro: la búsqueda de DNS continuará reintentando la búsqueda con el nuevo nombre.
>
>MX: Relaciona un nombre de ámbito a una lista de agentes de transferencia del mensaje para aquel ámbito.
>
>TXT: Creado originalmente para albergar texto a ser leído por humanos.
- ¿Qué hacen las herramientas `nslookup` y `dig`?
>nslookup: es un programa utilizado para saber si el DNS está resolviendo correctamente los nombres y las IPs.
>
>dig: permite realizar consultas a servidores DNS para obtener información acerca de sus registros.

### 3. Packet Tracer

Capturas mostrando el proceso de configuración paso a paso de la topología señalada:

![](imgs/1.png)

![](imgs/2.png)

![](imgs/3.png)

![](imgs/4.png)

![](imgs/5.png)

![](imgs/6.png)

![](imgs/7.png)

![](imgs/8.png)

![](imgs/9.png)

![](imgs/10.png)

![](imgs/11.png)

![](imgs/12.png)

![](imgs/13.png)

![](imgs/14.png)

![](imgs/15.png)

![](imgs/16.png)

![](imgs/17.png)

![](imgs/18.png)

![](imgs/19.png)

![](imgs/20.png)

![](imgs/21.png)

Una vez completado la topolgía se comprueba que PC0 y PC2 (en diferentes segmentos de IP) pueden resolver correctamente el nombre de domino `www.redes.net` y acceder al servidor HTTP indicado por la IP resuelta:

![](imgs/22.png)

![](imgs/23.png)

### Archivo de PacketTracer

[DNS.pkt](packettracer/dns.pkt)

#### Preguntas

- ¿Qué diferencia hay entre un switch y un router?
> Un switch está diseñado para conectar computadoras en una red, mientras que un router está diseñado para conectar múltiplas redes.
- ¿Qué tipos de switches hay?
> Switch troncal/perimetral, gestionable/no gestionable, desktop, perimetral gestionable/no gestionable, apilable/no apilable, store and forward, cut through/cut through adaptivo.
- ¿Qué es una lista de acceso y para qué se utilizan?
> Es una forma de determinar los permisos de acceso apropiados a un determinado objeto, dependiendo de ciertos aspectos del proceso que hace el pedido. Su principal objetivo es filtrar tráfico, permitiendo o denegando el tráfico de red de acuerdo a alguna condición.
- Se creó una lista de acceso con el comando access-list 1 permit 192.168.1.0 0.0.0.255, la última parte del comando (0.0.0.255) es una wildcard, ¿qué es una wildcard  y en qué se diferencia con una máscara de red?
> Las wildcards definen un host o un número de hosts en una subred u octeto y son mucho más flexibles que las máscaras de red, ya que no hay requisito de que los bits a 1 estén continuos. Otro punto que se diferencia es que los bits a tener en cuenta son al contrario que las máscaras de red, es decir, que el que tiene peso es el 0 y no el 1.
- ¿Qué diferencia hay entre una dirección IP pública y una privada?
>La IP pública es el identificador de la red que es el que es visible desde fuera, mientras que la privada es la que identifica a cada uno de los dispositivos conectados a la red local.
- ¿Qué diferencia hay entre NAT estática, NAT dinámica y PAT? ¿Cuál es la que se encuentra usualmente en los routers de los hogares?
>NAT estática: Se usa para realizar una relación uno a unoentre una dirección interna y una externa. También permite las conexiónes de un host externo a un host interno.
>
>NAT dinámica: Establece la relación de una dirección IP privada a una dirección IP publica tomada de un conjunto de direcciones IP públicas.
>
>PAT: Permite relacionar varias direcciones IP privadas a una sola direccion IP publica usando la traducción de direcciones de puertos. Es la maás usada por los routers en los hogares por su naturaleza.