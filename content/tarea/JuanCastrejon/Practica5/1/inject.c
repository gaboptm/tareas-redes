#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <pcap.h>

int main(int argc, char **argv)
{
  pcap_t *handle;
  char errbuf[PCAP_ERRBUF_SIZE];
  u_char packet[100];

  if (argc != 4)
  {
    printf("Uso: %s <interfaz> <MAC destino> <MAC origen>\n", argv[0]);
    return 0;
  }

  handle = pcap_open_live(argv[1], 100, 1, 1000,errbuf);
  if (handle == NULL)
  {
    perror(errbuf);
    return 0;
  }

  char *ptr = strtok(argv[2], ":");
  int i = 0;
  while(ptr != NULL) {
    packet[i++] = (int)strtol(ptr, NULL, 16);
    ptr = strtok(NULL, ":");
  }

  char *ptr2 = strtok(argv[3], ":");
  i = 6;
  while(ptr2 != NULL) {
    packet[i++] = (int)strtol(ptr2, NULL, 16);
    ptr2 = strtok(NULL, ":");
  }

  memcpy(packet + 14, "Custom Payload Injected", 23);

  if (pcap_inject(handle, packet, 100) < 0)
  {
    perror(pcap_geterr(handle));
  }

  return 0;
}