# Práctica 3: Manejo de archivos binarios y captura de paquetes

## Ejercicios

### 1. file

Comparación de resultados de `./file` y el binario `file` de unix:

![Ejemplo 1](imgs/1.png)

**Nota**: Para generar los archivos vacíos sólo con la información de su número mágico, se usó:
```
$ echo -n -e '\x47\x49\x46\x38\x37\x61' > emptygif
```
donde `-n` indica a echo no agregar un salto de linea al final de la cadena, `-e` habilita las secuencias de escape y `'\x47\x49\x46\x38\x37\x61'` es la cadena con valores escapados en hexadecimal que indica el número mágico para un archivo GIF.

### 2. strings

Comparación de resultados de `./strings` y el binario `xxd` de unix que hace un volcado de los valores hexadecimales en un archivo.

![Ejemplo 2](imgs/2.png)

### 3. Hashes

Comparación de resultados de `./hash` y el binario `openssl` de la biblioteca *libopenssl* para los hashes **md5**, **sha1**, **sha256**.

![Ejemplo 3](imgs/3.png)

### 4. Filtros

- Mostrar paquetes que tengan por dirección MAC origen 60:a4:4c:89:3c:a4 o que tengan por dirección MAC destino fc:fb:fb:01:fa:21.
```
$ tcpdump ether host 60:a4:4c:89:3c:a4 or ether dst fc:fb:fb:01:fa:21
```
- Mostrar paqutes cuya dirección MAC ya sea origen o destino comience con 88:53:95.
```
$ tcpdump "ether [0:4] & 0xffffff00 == 0x88539500 or ether [6:4] & 0xffffff00 == 0x88539500"
```
- Mostrar paquetes cuyo ethertype sea IPv6.
```
$ tcpdump ether proto 0x86DD
```
- Mostrar paqutes que tengan por ethertype a ARP y
cuya dirección MAC destino sea ff:ff:ff:ff:ff:ff.
```
$ tcpdump ether proto 0x0806 and ether dst ff:ff:ff:ff:ff:ff
```
- Mostrar paquetes cuya dirección IP empieza con 172.
```
$ tcpdump net 172.0.0.0/8
```

### e1. Captura Wireshark

- ¿Encontraste algo sospechoso en la captura de tráfico? ¿Si así fue, cuál es la contraseña y usuario que fueron dadas a conocer?

>Se usó curl para mandar un get a la dirección http://172.16.16.1/secreto
  donde se realizó un Authorization Basic con las credenciales prueba:hola123,
  luego se descargó un arhivo con extensión zip que contenía un archivo binario
  con las credenciales user=david&password=blackstar

- ¿Cuáles son las direcciones MAC de los dos equipos que estaban comunicándose?

>Fuente: 00:0c:29:79:af:e0, Destino: 00:50:56:c0:00:08

- ¿Cuáles son sus direcciones IP?

>Fuente: 172.16.16.217, Destino: 172.16.16.1

- ¿Cuál es el hash MD5 del archivo escondido en la captura?

>MD5 hash: 6fe658c629bcb4327d6c475e06d899fc

## Conclusiones

Se rectificó el manejo binario de archivos, el concepto de hash y se aprendió a usar la biblioteca *libopenssl*. También se aprendió el uso de las herramientas *tcpdump* y *Wireshark* para captura de tráfico, y su análisis.
